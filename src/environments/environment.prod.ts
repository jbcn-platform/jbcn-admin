export const environment = {
    production: true,
    apiUrlBase: 'https://apijbcn.cleverapps.io/api',
    apiPublicBase: 'https://apijbcn.cleverapps.io/public',
    loginUrl: 'https://apijbcn.cleverapps.io/login',
    logoutUrl: 'https://apijbcn.cleverapps.io/logout',
    publicView: 'https://papers.jbcnconf.com/paper',
    apiHost: 'https://apijbcn.cleverapps.io/',
    edition: {
        name: 'JBCNConf',
        year: '2022',
        plannedTalks: '64 (TBC)',
        plannedWorkshops: '10',
    }
};
