// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    apiHost: 'http://localhost:8080/',
    apiUrlBase: `http://localhost:8080/api`,
    apiPublicBase: 'http://localhost:8080/public',
    loginUrl: 'http://localhost:8080/login',
    logoutUrl: 'http://localhost:8080/logout',
    publicView: 'https://papers.jbcnconf.com/paper',
    edition: {
        name: 'JBCNConf',
        year: '2022',
        plannedTalks: '64 (TBC)',
        plannedWorkshops: '10',
    }
};
