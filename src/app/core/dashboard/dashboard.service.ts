import {Injectable} from '@angular/core';
import {ApiService} from '@core/api/api.service';
import {AuthService} from '@core/auth/auth.service';
import {ErrorService} from '@core/error/error.service';
import {environment} from 'environments/environment';

@Injectable()
export class DashboardService {

    constructor(
        private apiService: ApiService,
        private authService: AuthService,
        private errorService: ErrorService
    ) {
    }

    countTags(edition: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/counter/tag/${edition}`;
            this.apiService.doGet(url, token).subscribe((response) => {
                if (response.status) {
                    resolve(response.data);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }
}
