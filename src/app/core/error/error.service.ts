import {EventEmitter, Injectable} from '@angular/core';
import {ToasterService} from 'angular2-toaster/angular2-toaster';
import {Router} from '@angular/router';
import {AuthService} from '@core/auth/auth.service';

@Injectable()
export class ErrorService {

    public errorEvents = new EventEmitter<any>();
    public successEvents = new EventEmitter<any>();

    constructor(
        private toasterService: ToasterService,
        private router: Router,
        private authService: AuthService
    ) {
        this.errorEvents.subscribe(error => {
            this.toasterService.pop('error', 'Error!', error);
        });

        this.successEvents.subscribe(message => {
            this.toasterService.pop('success', 'Success!', message);
        });
    }

    public handleError(error: any) {
        if (error.status === 401) {
            this.authService.logout();
            this.router.navigate(['login']);
        } else {
            if (error._body) {
                this.errorEvents.emit(error._body);
            } else {
                this.errorEvents.emit(error);
            }
        }
    }
}
