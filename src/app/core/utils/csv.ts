import {saveAs} from 'file-saver';

export class Csv {

    private values = Array<any>();

    static of(values: Object[]): Csv {
        return new Csv(values);
    }

    private constructor(values: Object[]) {
        this.values = values;
    }

    public line(values: any[]): string {
        return values
            .map(it => `"${it}"`)
            .join(',') + '\n';
    }

    public saveAsCSV(filename: string, headers: string[], mapper: (any) => any[]) {
        let buffer = this.line(headers);
        this.values.map(it => {
            buffer += this.line(mapper(it));
        });
        const blob = new Blob([buffer], {type: 'text/csv;charset=utf-8'});
        saveAs(blob, filename);
    }
}
