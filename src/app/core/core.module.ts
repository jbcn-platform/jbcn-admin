import {NgModule, Optional, SkipSelf} from '@angular/core';
import {HttpModule} from '@angular/http';
import {ApiService} from './api/api.service';
import {SettingsService} from './settings/settings.service';
import {ThemesService} from './themes/themes.service';
import {TranslatorService} from './translator/translator.service';
import {MenuService} from './menu/menu.service';
import {AuthService} from './auth/auth.service';
import {throwIfAlreadyLoaded} from './module-import-guard';
import {DashboardService} from './dashboard/dashboard.service';
import {ErrorService} from './error/error.service';
import {UserService} from './backend/user.service';
import {PaperService} from './backend/paper.service';
import {TalkService} from './backend/talk.service';
import {PaperStreamService} from './backend/paper-stream.service';
import {CompanyService} from './backend/company.service';
import {MailService} from '@core/backend/mail.service';

@NgModule({
    imports: [
        HttpModule
    ],
    providers: [
        ApiService,
        SettingsService,
        ThemesService,
        TranslatorService,
        MenuService,
        AuthService,
        UserService,
        PaperService,
        CompanyService,
        MailService,
        ErrorService,
        PaperStreamService,
        TalkService,
        DashboardService,
    ],
    declarations: [],
    exports: []
})

export class CoreModule {
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }
}
