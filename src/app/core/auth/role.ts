export class Role {
    static ADMIN = "admin"
    static VOTER = "voter"
    static HELPER = "helper"
    static SPONSOR = "sponsor"
}