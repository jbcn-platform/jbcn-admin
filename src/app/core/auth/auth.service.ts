import {EventEmitter, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '@core/api/api.service';
import {environment} from 'environments/environment';
import {Role} from './role';

const TOKEN_FIELD = 'token';
const USERNAME_FIELD = 'jbcnUsername';
const USER_PERMISSIONS = 'user.roles';

@Injectable()
export class AuthService {

    private AUTHORIZATION_PREFIX = 'role:';

    public authEventEmitter = new EventEmitter<boolean>();

    constructor(private apiService: ApiService, private router: Router) {
    }

    getCurrentToken() {
        return sessionStorage.getItem(TOKEN_FIELD);
    }

    isLogged() {
        return sessionStorage.getItem(TOKEN_FIELD) != null;
    }

    putCurrentToken(token: string) {
        sessionStorage.setItem(TOKEN_FIELD, token);
    }

    putCurrentUsername(username: string) {
        sessionStorage.setItem(USERNAME_FIELD, username);
    }

    putCurrentUserPermissions(permissions: string) {
        sessionStorage.setItem(USER_PERMISSIONS, permissions);
    }

    clearCurrentToken() {
        sessionStorage.removeItem(TOKEN_FIELD);
    }

    clearSession() {
        sessionStorage.clear();
    }

    getCurrentUsername(): string {
        return sessionStorage.getItem(USERNAME_FIELD);
    }

    getCurrentUserPermissions(): string[] {
        return JSON.parse(sessionStorage.getItem(USER_PERMISSIONS));
    }

    hasRole(...roles: Role[]): boolean {
        const permissions = this.getCurrentUserPermissions();
        for (const role of roles) {
            if (permissions.includes(this.AUTHORIZATION_PREFIX + role)) {
                return true;
            }
        }
        return false;
    }

    checkSession(): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            if (!this.getCurrentToken()) {
                resolve(false);
            } else {
                this.apiService.doGet(`${environment.apiUrlBase}/checkSession`, this.getCurrentToken()).subscribe(response => {
                    if (response.status) {
                        resolve(true);
                    } else {
                        this.clearCurrentToken();
                        resolve(false);
                    }
                }, error => {
                    this.clearCurrentToken();
                    resolve(false);
                });
            }
        });
    }

    login(username: string, password: string): Promise<string> {
        const params = {
            'username': username,
            'password': password
        };
        return new Promise<string>((resolve, reject) => {
            this.apiService.doPost(environment.loginUrl, null, params).subscribe(response => {
                if (response['status']) {
                    const token = response.data.token;
                    const payload = atob(token.split('.')[1]);
                    const userData = JSON.parse(payload);
                    this.clearSession();
                    this.putCurrentToken(token);
                    this.putCurrentUsername(userData.sub);
                    this.putCurrentUserPermissions(JSON.stringify(userData.permissions));
                    resolve(token);
                    this.authEventEmitter.emit(true);
                } else {
                    reject(response['error']);
                }
            }, error => {
                console.error('Error in login', error);
                reject(error);
            });

        });
    }

    logout() {
        const token = this.getCurrentToken();
        this.apiService.doGet(environment.logoutUrl, token).subscribe(response => {
            console.log('logout: ', response);
        }, error => {
            console.error('logout error:', error);
        });
        this.clearSession();
        this.authEventEmitter.emit(false);
        this.router.navigate(['login']);
    }

}
