import {Injectable} from '@angular/core';
import {AuthService} from '@core/auth/auth.service';
import {MenuItem} from '@core/menu/menuItem';

@Injectable()
export class MenuService {

    menuItems: Array<any>;

    constructor(
        protected authService: AuthService,
    ) {
        this.menuItems = [];
    }

    addMenu(items: MenuItem[]) {
        const permissions = this.authService.getCurrentUserPermissions();
        items.forEach((item) => {
            if (this.hasAnyPermission(item, permissions)) {
                this.menuItems.push(item);
            }
        });
    }

    getMenu() {
        return this.menuItems;
    }

    private hasAnyPermission(item: MenuItem, permissions: string[]): boolean {
        if (item.permissions && permissions) {
            for (const itemPermission of item.permissions) {
                if (permissions.includes(itemPermission)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
}

