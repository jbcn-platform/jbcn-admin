import {Injectable} from '@angular/core';
import {ApiService} from '@core/api/api.service';
import {AuthService} from '@core/auth/auth.service';
import {ErrorService} from '@core/error/error.service';
import {AbstractCrudService} from './abstractCrud.service';
import {Sponsor} from '@shared/model/badge';

// FIXME no longer working with new implementation
// https://gitlab.com/jbcn-platform/jbcn-api/-/commit/46da9b70e19ce307e5b41d53dea38275ed1b3a75
@Injectable()
export class SponsorsService extends AbstractCrudService<Sponsor> {

    constructor(apiService: ApiService, authService: AuthService, errorService: ErrorService) {
        super(apiService, authService, errorService);
    }

    endpoint: String = 'sponsors';

}
