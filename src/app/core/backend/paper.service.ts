import {Injectable} from '@angular/core';
import {Paper, PaperNote, PaperVote, SearchResult, User} from '@shared/model/models';
import {ApiService} from '@core/api/api.service';
import {ErrorService} from '@core/error/error.service';
import {AuthService} from '@core/auth/auth.service';
import {environment} from '../../../environments/environment';
import {SearchConditions, SearchFilter} from '@shared/model/searchParams';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class PaperService {

    constructor(private apiService: ApiService, private errorService: ErrorService, private authService: AuthService) {
    }

    save(paper: Paper): Promise<boolean> {
        if (!paper._id) {
            return this.create(paper);
        } else {
            return this.update(paper);
        }
    }

    create(paper: Paper): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/paper`;
            this.apiService.doPost(url, token, paper).subscribe(response => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject(false);
                }
            }, error => {
                this.errorService.handleError(error);
                reject(false);
            });
        });
    }


    getNotes(id: string): Promise<Array<PaperNote>> {
        return new Promise<Array<PaperNote>>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/notes?paperId=${id}`;
            this.apiService.doGet(url, token).subscribe(response => {
                if (response.status) {
                    if (response.data.items) {
                        resolve(response.data.items);
                    } else {
                        resolve([]);
                    }
                } else {
                    this.errorService.handleError(response.error);
                }
            }, error => {
                this.errorService.handleError(error);
                reject(false);
            });
        });
    }

    addNote(paperNote: PaperNote): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/notes`;
            this.apiService.doPost(url, token, paperNote).subscribe(response => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject(false);
                }
            }, error => {
                this.errorService.handleError(error);
                reject(false);
            });
        });
    }

    updateNote(paperNote: PaperNote): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/notes/${paperNote.id}`;
            this.apiService.doPut(url, token, paperNote).subscribe(response => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject(false);
                }
            }, error => {
                this.errorService.handleError(error);
                reject(false);
            });
        });
    }

    deleteNote(paperNote: PaperNote): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/notes/${paperNote.id}`;
            this.apiService.doDelete(url, token).subscribe(response => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject(false);
                }
            }, error => {
                this.errorService.handleError(error);
                reject(false);
            });
        });
    }

    /* To delete */
    // getMockNotes(): Array<PaperNote> {
    //     const result = [];
    //     for (let i = 0; i < 20; i++) {
    //         const note: PaperNote = new PaperNote();
    //         note._id = 'NOTE_ID';
    //         note.owner = 'username';
    //         note.lastUpdate = new Date();
    //         note.createdDate = new Date();
    //         note.paperId = 'PAPER_  ID';
    //         note.text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    //         result.push(note);
    //     }
    //     return result;
    // }

    update(paper: Paper): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/paper/${paper._id}`;
            delete paper._id;
            this.apiService.doPut(url, token, paper).subscribe(response => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject(false);
                }
            }, error => {
                this.errorService.handleError(error);
                reject(false);
            });
        });
    }

    setSponsor(id: string, sponsor: boolean): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/paper/${id}/sponsor/${sponsor}`;
            this.apiService.doPut(url, token, {}).subscribe(response => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject(false);
                }
            }, error => {
                this.errorService.handleError(error);
                reject(false);
            });
        });
    }

    count(edition: string): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/paper/count/${edition}`;
            this.apiService.doGet(url, token).subscribe(response => {
                if (response.status) {
                    const count = response.data.count;
                    resolve(count);
                } else {
                    this.errorService.handleError(response.error);
                }
            }, error => {
                this.errorService.handleError(error);
                reject(false);
            });
        });
    }

    approve(paper: Paper, name: string): Promise<boolean> {
        const person = new User();
        person.username = name;
        return new Promise<boolean>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/paper/${paper._id}/approve`;
            this.apiService.doPost(url, token, person).subscribe(response => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject(false);
                }
            }, error => {
                this.errorService.handleError(error);
                reject(false);
            });
        });
    }

    delete(id: String): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/paper/${id}`;
            this.apiService.doDelete(url, token).subscribe(response => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject(false);
                }
            }, error => {
                this.errorService.handleError(error);
                reject(false);
            });
        });
    }

    search(conditions: SearchConditions,
           page: number, size: number, sort: string, asc: boolean
    ): Promise<SearchResult<Paper>> {
        return new Promise<SearchResult<Paper>>((resolve, reject) => {
            const url = `${environment.apiUrlBase}/paper/search`;
            const token = this.authService.getCurrentToken();
            const payload = {
                page: page,
                size: size,
                sort: sort,
                asc: asc,
                conditions: conditions
            };
            this.apiService.doPost(url, token, payload).subscribe((response) => {
                if (response.status) {
                    resolve(response.data);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    // deprecated
    formSearch(term: string, state: string, type: string, includeSponsor: boolean,
               excludeSpeakers: boolean, company: string, speakerName: string,
               vote: PaperVote,
               page: number, size: number, sort: string, asc: boolean): Promise<SearchResult<Paper>> {
        const filters = [];
        // special options
        if (term && term !== '') {
            filters.push(SearchFilter.equals('term', term));
        }
        // paper fields
        if (state && state !== '') {
            filters.push(SearchFilter.equals('state', state));
        }
        if (type && type !== '') {
            filters.push(SearchFilter.equals('type', type));
        }
        if (includeSponsor) {
            filters.push(SearchFilter.equals('sponsor', true));
        }
        // sender fields
        if (excludeSpeakers) {
            filters.push(SearchFilter.equals('not-published-speakers', true));
        }
        if (company && company !== '') {
            filters.push(SearchFilter.equals('sender.company', company));
        }
        if (speakerName && speakerName !== '') {
            filters.push(SearchFilter.equals('sender.fullName', speakerName));
        }
        // voting
        if (vote && (vote.username !== '' || (vote.vote != null && !isNaN(vote.vote)))) {
            filters.push(SearchFilter.vote(vote.username, vote.vote));
        }
        return this.search({all: filters}, page, size, sort, asc);
    }

    get(id: string): Promise<Paper> {
        return new Promise<Paper>((resolve, reject) => {
            const url = `${environment.apiUrlBase}/paper/${id}`;
            const token = this.authService.getCurrentToken();
            this.apiService.doGet(url, token).subscribe(response => {
                if (response.status) {
                    resolve(response.data.instance);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    stringArrayAsText(values: Array<string>): string {
        let result: string;
        if (values && values.length > 0) {
            result = '';
            for (const value of values) {
                if (result.length > 0) {
                    result = result + ', ';
                }
                result = result + value;
            }
        }
        return result;
    }

    addFeedback(id: string, text: string) {
        const url = `${environment.apiUrlBase}/paper/${id}/feedback`;
        const token = this.authService.getCurrentToken();
        const payload = {text: text};
        let apiCall = this.apiService.doPost(url, token, payload);
        return this.feedbackCall(apiCall);
    }

    updateFeedback(id: string, text: string) {
        const url = `${environment.apiUrlBase}/paper/${id}/feedback`;
        const token = this.authService.getCurrentToken();
        const payload = {text: text};
        let apiCall = this.apiService.doPut(url, token, payload);
        return this.feedbackCall(apiCall);
    }

    private feedbackCall(apiCall: Observable<any>) {
        return new Promise<Paper>((resolve, reject) => {
            apiCall.subscribe(response => {
                if (response.status) {
                    resolve(response.data);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }
}
