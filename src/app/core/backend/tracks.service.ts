import { Injectable } from '@angular/core';
import { Track } from '@shared/model/scheduling';
import { ApiService } from '@core/api/api.service';
import { AuthService } from '@core/auth/auth.service';
import { ErrorService } from '@core/error/error.service';
import { AbstractCrudService } from './abstractCrud.service';

@Injectable()
export class TracksService extends AbstractCrudService<Track> {

    constructor(apiService: ApiService, authService: AuthService, errorService: ErrorService) {
        super(apiService, authService, errorService)
    }

    endpoint: String = "tracks";

}
