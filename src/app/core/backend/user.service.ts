import { Injectable } from '@angular/core';
import { User, SearchResult } from '@shared/model/models';
import { ApiService } from '@core/api/api.service';
import { AuthService } from '@core/auth/auth.service';
import { ErrorService } from '@core/error/error.service';
import { environment } from 'environments/environment';


@Injectable()
export class UserService {

    constructor(
        private apiService: ApiService,
        private authService: AuthService,
        private errorService: ErrorService
    ) { }

    search(term: string, page: number, size: number, sort: string, asc: boolean): Promise<SearchResult<User>> {

        return new Promise<SearchResult<User>>((resolve, reject) => {

            const url = `${environment.apiUrlBase}/users/search`;
            const token = this.authService.getCurrentToken();
            const payload = {
                page: page,
                size: size,
                term: term,
                sort: sort,
                asc: asc
            };
            this.apiService.doPost(url, token, payload).subscribe((response) => {
                if (response.status) {
                    resolve(response.data);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }

            }, error => {
                this.errorService.handleError(error);
                reject();
            });

        });
    }

    get(id: string): Promise<User> {
        return new Promise<User>((resolve, reject) => {
            const url = `${environment.apiUrlBase}/users/${id}`;
            const token = this.authService.getCurrentToken();
            this.apiService.doGet(url, token).subscribe(response => {
                if (response.status) {
                    resolve(response.data);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    save(user: User): Promise<boolean> {
        if (user._id) {
            return this.update(user);
        } else {
            return this.create(user);
        }
    }

    create(user: User): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const url = `${environment.apiUrlBase}/users`;
            const token = this.authService.getCurrentToken();
            this.apiService.doPost(url, token, user).subscribe(response => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    delete(user: User): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const url = `${environment.apiUrlBase}/users/${user._id}`;
            const token = this.authService.getCurrentToken();
            this.apiService.doDelete(url, token).subscribe(response => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    update(user: User): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const url = `${environment.apiUrlBase}/users/${user._id}`;
            const token = this.authService.getCurrentToken();
            this.apiService.doPut(url, token, user).subscribe(response => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    getVoters(): Promise<Array<User>> {
        return new Promise<Array<User>>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/voters`;
            this.apiService.doGet(url, token).subscribe(response => {
                if (response.status) {
                    if (response.data.items) {
                        resolve(response.data.items);
                    } else {
                        resolve([]);
                    }
                } else {
                    this.errorService.handleError(response.error);
                }
            }, error => {
                this.errorService.handleError(error);
                reject(false);
            });
        });
    }

}
