import {Injectable} from '@angular/core';
import {ApiService} from '@core/api/api.service';
import {AuthService} from '@core/auth/auth.service';
import {ErrorService} from '@core/error/error.service';
import {environment} from 'environments/environment';
import {Company} from '@shared/model/company';

@Injectable()
export class CompanyService {

    constructor(private apiService: ApiService, private authService: AuthService, private errorService: ErrorService) {
    }

    getAll(proposalPhase: string): Promise<Company[]> {
        return new Promise<Company[]>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/companies?proposalPhase=${proposalPhase}`;
            this.apiService.doGet(url, token).subscribe((response) => {
                if (response.status) {
                    resolve(response.data.items);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

}
