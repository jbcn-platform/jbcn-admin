import { Injectable } from '@angular/core';
import { Talk, SearchResult, ISearchParams } from '@shared/model/models';
import { ApiService } from '@core/api/api.service';
import { AuthService } from '@core/auth/auth.service';
import { ErrorService } from '@core/error/error.service';
import { environment } from 'environments/environment';

@Injectable()
export class TalkService {

    constructor(
        private apiService: ApiService,
        private authService: AuthService,
        private errorService: ErrorService
    ) { }


    search(params: ISearchParams): Promise<SearchResult<Talk>> {
        const url = `${environment.apiUrlBase}/talk/search`;
        const token = this.authService.getCurrentToken();

        return new Promise<SearchResult<Talk>>((resolve, reject) => {
            this.apiService.doPost(url, token, params).subscribe((response) => {
                if (response.status) {
                    resolve(response.data);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    get(id: string): Promise<Talk> {
        return new Promise<Talk>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/talk/${id}`;
            this.apiService.doGet(url, token).subscribe((response) => {
                if (response.status) {
                    resolve(response.data.instance);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    count(edition: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/talk/count/${edition}`;
            this.apiService.doGet(url, token).subscribe((response) => {
                if (response.status) {
                    resolve(response.data);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    updateState(talk: Talk, state: string): Promise<boolean> {
        const url = `${environment.apiUrlBase}/talk/${talk._id}/state`;
        const payload = {
            'state': state
        };
        const token = this.authService.getCurrentToken();
        return new Promise<boolean>((resolve, reject) => {
            this.apiService.doPut(url, token, payload).subscribe((response) => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    updatePublished(talk: Talk, published: string): Promise<boolean> {
        const url = `${environment.apiUrlBase}/talk/${talk._id}/published`;
        const payload = {
            'published': published
        };
        const token = this.authService.getCurrentToken();
        return new Promise<boolean>((resolve, reject) => {
            this.apiService.doPut(url, token, payload).subscribe((response) => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    update(talk: Talk): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            const url = `${environment.apiUrlBase}/talk/${talk._id}`;
            this.apiService.doPut(url, token, talk).subscribe(response => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject(false);
                }
            }, error => {
                this.errorService.handleError(error);
                reject(false);
            });
        });
    }

    stringArrayAsText(values: Array<string>): string {
        let result: string;
        if (values && values.length > 0) {
            result = '';
            for (const value of values) {
                if (result.length > 0) {
                    result = result + ', ';
                }
                result = result + value;
            }
        }
        return result;
    }

}
