import { Injectable } from '@angular/core';
import { Vote } from '@shared/model/vote';
import { ApiService } from '@core/api/api.service';
import { AuthService } from '@core/auth/auth.service';
import { ErrorService } from '@core/error/error.service';
import { AbstractCrudService } from '@core/backend/abstractCrud.service';

@Injectable()
export class VotesService extends AbstractCrudService<Vote> {

    constructor(apiService: ApiService, authService: AuthService, errorService: ErrorService) {
        super(apiService, authService, errorService)
    }

    endpoint: String = "attendees/votes";

}
