import { Injectable } from '@angular/core';
import { ApiService } from '@core/api/api.service';
import { AuthService } from '@core/auth/auth.service';
import { ErrorService } from '@core/error/error.service';
import { AbstractCrudService } from '@core/backend/abstractCrud.service';
import { Badge } from '@shared/model/badge';

@Injectable()
export class BadgesService extends AbstractCrudService<Badge> {

    constructor(apiService: ApiService, authService: AuthService, errorService: ErrorService) {
        super(apiService, authService, errorService)
    }

    endpoint: String = "badges";

}
