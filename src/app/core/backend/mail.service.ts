import {Injectable} from '@angular/core';
import {SearchResult, User} from '@shared/model/models';
import {ApiService} from '@core/api/api.service';
import {AuthService} from '@core/auth/auth.service';
import {ErrorService} from '@core/error/error.service';
import {environment} from 'environments/environment';


@Injectable()
export class MailService {

    constructor(
        private apiService: ApiService,
        private authService: AuthService,
        private errorService: ErrorService
    ) {
    }

    sendFeedback(paperId: string): Promise<SearchResult<User>> {
        return new Promise<SearchResult<User>>((resolve, reject) => {
            const type = 'paper-feedback';
            const url = `${environment.apiUrlBase}/emails/${type}`;
            const token = this.authService.getCurrentToken();
            const payload = {
                'paper-id': paperId
            };
            this.apiService.doPost(url, token, payload).subscribe((response) => {
                console.log(`response ${response}`)
                if (response.status != 202) {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }
}
