import {Injectable} from '@angular/core';
import {ApiService} from '@core/api/api.service';
import {AuthService} from '@core/auth/auth.service';
import {ErrorService} from '@core/error/error.service';
import {AbstractCrudService} from './abstractCrud.service';
import {BackofficeSponsor} from '@shared/model/backoffice';

@Injectable()
export class BackofficeSponsorsService extends AbstractCrudService<BackofficeSponsor> {

    constructor(apiService: ApiService, authService: AuthService, errorService: ErrorService) {
        super(apiService, authService, errorService);
    }

    endpoint: String = 'sponsors';

}
