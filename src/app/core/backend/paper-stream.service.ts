import {Injectable} from '@angular/core';
import {Paper} from '@shared/model/models';
import {AuthService} from '@core/auth/auth.service';
import {ApiService} from '@core/api/api.service';
import {ErrorService} from '@core/error/error.service';
import {environment} from 'environments/environment';

@Injectable()
export class PaperStreamService {

    constructor(
        private apiService: ApiService,
        private authService: AuthService,
        private errorService: ErrorService) {
    }

    getPaperStream(): Promise<Array<Paper>> {
        const token = this.authService.getCurrentToken();
        return new Promise<Array<Paper>>((resolve, reject) => {
            this.apiService.doGet(`${environment.apiUrlBase}/vote/stream`, token)
                .subscribe(response => {
                    if (response['status']) {
                        resolve(response['data']);
                    } else {
                        this.errorService.handleError(response['error']);
                        reject(response['error']);
                    }
                }, error => {
                    this.errorService.handleError(error);
                    reject(null);
                });
        });
    }

    vote(paperId: string, vote: number) {
        return new Promise<boolean>((resolve, reject) => {
            const payload = {
                id: paperId,
                vote: vote
            };
            const token = this.authService.getCurrentToken();
            this.apiService.doPost(`${environment.apiUrlBase}/vote`, token, payload).subscribe(
                response => {
                    if (response['status']) {
                        resolve(true);
                    } else {
                        this.errorService.handleError(response['error']);
                        reject(false);
                    }
                },
                error => {
                    this.errorService.handleError(error);
                    reject(false);
                }
            );
        });
    }

}
