import {Traceable} from '@shared/model/models';
import {ApiService} from '@core/api/api.service';
import {AuthService} from '@core/auth/auth.service';
import {ErrorService} from '@core/error/error.service';
import {environment} from 'environments/environment';

export abstract class AbstractCrudService<T extends Traceable> {

    protected constructor(
        protected apiService: ApiService,
        private authService: AuthService,
        private errorService: ErrorService) {
    }

    abstract endpoint: String;

    private url(): string {
        return `${environment.apiUrlBase}/${this.endpoint}`;
    }

    create(instance: T): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            this.apiService.doPost(this.url(), token, instance).subscribe((response) => {
                if (response.status) {
                    resolve(response.data._id);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    get(id: string): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            this.apiService.doGet(`${this.url()}/${id}`, token).subscribe((response) => {
                if (response.status) {
                    resolve(response.data);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    getAll(): Promise<T[]> {
        return new Promise<T[]>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            this.apiService.doGet(this.url(), token).subscribe((response) => {
                if (response.status) {
                    resolve(response.data.items);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    update(instance: T): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            this.apiService.doPut(`${this.url()}/${instance._id}`, token, instance).subscribe((response) => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

    delete(id: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const token = this.authService.getCurrentToken();
            this.apiService.doDelete(`${this.url()}/${id}`, token).subscribe((response) => {
                if (response.status) {
                    resolve(true);
                } else {
                    this.errorService.handleError(response.error);
                    reject();
                }
            }, error => {
                this.errorService.handleError(error);
                reject();
            });
        });
    }

}
