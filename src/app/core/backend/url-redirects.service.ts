import {Injectable} from '@angular/core';
import {ApiService} from '@core/api/api.service';
import {AuthService} from '@core/auth/auth.service';
import {ErrorService} from '@core/error/error.service';
import {AbstractCrudService} from './abstractCrud.service';
import {UrlRedirect} from '@shared/model/urlRedirect';

@Injectable()
export class UrlRedirectsService extends AbstractCrudService<UrlRedirect> {

    constructor(apiService: ApiService, authService: AuthService, errorService: ErrorService) {
        super(apiService, authService, errorService);
    }

    endpoint: String = 'url-redirects';
}
