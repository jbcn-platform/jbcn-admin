export class SearchComponent {

    public getStoredParams(name: string) {
        const values = sessionStorage.getItem(name);
        if (values != null) {
            return JSON.parse(values);
        }
        return null;
    }

    public putStoredParams(name: string, values: any) {
        sessionStorage.setItem(name, JSON.stringify(values));
    }

}
