import { Traceable } from './traceable';
import { Talk } from './talk';

export class Vote extends Traceable {
    constructor() {
        super()
    }

    talkId: string
    userEmail: number
    // 1 to 5
    value: number
    // emoji identifier for additional feedback
    delivery:string
    // feedback text
    other:string

    // added for UI
    talk?: Talk
}
