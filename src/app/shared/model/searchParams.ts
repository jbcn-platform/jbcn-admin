export class SearchFilter {
    name: string
    value: any
    op: string

    static equals(name: string, value: any): SearchFilter {
        return new SearchFilter(name, value, null)
    }

    static contains(name: string, value: any): SearchFilter {
        return new SearchFilter(name, value, 'contains')
    }

    static vote(username: string, vote: number): SearchFilter {
        return new SearchFilter('vote', { 'username': username, 'vote': vote }, null)
    }

    private constructor(name: string, value: any, op: string) {
        this.name = name
        this.value = value
        this.op = op
    }

}

export interface SearchConditions {
    all?: SearchFilter[]
    any?: SearchFilter[]
}

export interface PapersSearchParams {
    page?: number;
    size: number;
    sort: string;
    asc: boolean;
    conditions?: SearchConditions;
}

export interface ISearchParams {
    term?: string;
    page?: number;
    size: number;
    sort: string;
    asc: boolean;
    filters?: SearchFilter[];
    fields?: string[];
}
