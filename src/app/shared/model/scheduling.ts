import { Traceable } from './traceable';
import { Talk } from './talk';

export class Room extends Traceable {
    constructor() {
        super();
    }

    name: string;
    capacity: number;
}

export class Track extends Traceable {
    constructor() {
        super();
    }

    static of(id: string, name: string, description: string = null): Track {
        let t = new Track()
        t._id = id
        t.name = name
        t.description = description
        return t;
    }

    name: string;
    description?: string;
}

export class Session extends Traceable {
    constructor() {
        super()
    }
    talks: Talk[]

    slotId: string
    day: string
    startTime: string
    endTime: string
    trackId: string
    roomId: string
    // added for UI
    room: Room
    track: Track

}
