import {Traceable} from './traceable';

export class UrlRedirect extends Traceable {
    url: string;
    name: string;
}
