export class SearchResult<T> {
    constructor() {
        this.items = [];
    }

    page: number;
    size: number;
    total: number;
    totalPages: number;
    items: Array<T>;
    votersCount: number;
}
