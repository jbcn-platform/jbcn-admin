import {Session} from './scheduling';
import {Traceable} from './traceable';

export class Talk extends Traceable {
    constructor() {
        super();
    }

    _id: string;
    edition: string;
    paperId: string;
    speakers: Speaker[];
    paperAbstract: string;
    comments: string;
    title: string;
    state: string;
    published: boolean;
    level: string;
    type: string;
    languages: string[];
    date: Date;
    averageVote: number;
    votesCount: number;
    preferenceDay: string;
    tags: string[];
    responsiblePerson: string;
    sessionId: string;
    // added for UI
    session: Session;
}

export class Speaker {
    fullName: string;
    email: string;
    picture: string;
    biography: string;
    twitter: string;
    web: string;
    linkedin: string;
    tshirtSize: boolean;
    travelCost: boolean;
    attendeesParty: boolean;
    speakersParty: boolean;
}
