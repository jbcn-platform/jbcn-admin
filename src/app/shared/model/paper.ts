import {Traceable} from './traceable';

export class Paper extends Traceable {

    constructor() {
        super();
        this.senders = [];
    }

    edition: string;
    senders: Sender[];
    abstract: string;
    comments: string;
    title: string;
    state: string;
    level: string;
    type: string;
    languages: string[];
    averageVote: number;
    votesCount: number;
    preferenceDay: string;
    tags: string[];
    votes: PaperVote[];
    travelCost: boolean;
    sponsor: boolean;
    feedback: Feedback;
}

export class PaperVote {
    userId: string;
    username: string;
    vote: number;
    date: Date;

    constructor(username: string, value: number) {
        this.username = username;
        this.vote = value;
    }
}

export class Sender {
    fullName: string;
    email: string;
    picture: string;
    jobTitle: string;
    company: string;
    biography: string;
    twitter: string;
    web: string;
    linkedin: string;
    tshirtSize: boolean;
    travelCost: boolean;
    attendeesParty: boolean;
    speakersParty: boolean;
    allergies: string;
}

export class Feedback extends Traceable {
    text: string;

    constructor() {
        super();
    }
}
