import {Traceable} from './traceable';

export class BackofficeSponsor extends Traceable {
    name: string;
    description: string;
    level: string;
    links: Map<string, string>;
}

export class JobOffer extends Traceable {
    title: string;
    description: string;
    location: string;
    link: string;
    sponsorId: string;
}
