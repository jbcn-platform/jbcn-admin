export class PaperNote {
    id: string
    owner: string
    text: string
    paperId: string
    createdDate: Date
    lastUpdate: Date
}
