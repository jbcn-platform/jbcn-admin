import { Traceable } from "./traceable"

export class User extends Traceable {
    username: string
    email: string
    password: string
    roles: string[]
}
