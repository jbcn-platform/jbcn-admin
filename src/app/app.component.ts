import {Component, HostBinding, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '@core/auth/auth.service';
import {SettingsService} from '@core/settings/settings.service';
import {loader} from '@shared/util/eventEmitters';

declare var $: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    @HostBinding('class.layout-fixed') get isFixed() { return this.settings.layout.isFixed; };
    @HostBinding('class.aside-collapsed') get isCollapsed() { return this.settings.layout.isCollapsed; };
    @HostBinding('class.layout-boxed') get isBoxed() { return this.settings.layout.isBoxed; };
    @HostBinding('class.layout-fs') get useFullLayout() { return this.settings.layout.useFullLayout; };
    @HostBinding('class.hidden-footer') get hiddenFooter() { return this.settings.layout.hiddenFooter; };
    @HostBinding('class.layout-h') get horizontal() { return this.settings.layout.horizontal; };
    @HostBinding('class.aside-float') get isFloat() { return this.settings.layout.isFloat; };
    @HostBinding('class.offsidebar-open') get offsidebarOpen() { return this.settings.layout.offsidebarOpen; };
    @HostBinding('class.aside-toggled') get asideToggled() { return this.settings.layout.asideToggled; };
    @HostBinding('class.aside-collapsed-text') get isCollapsedText() { return this.settings.layout.isCollapsedText; };

    logged = false;
    loading: boolean;
    constructor(public settings: SettingsService,
        private authService: AuthService,
        private router: Router) {
        loader.subscribe(value => {
            setTimeout(() => { this.changeLoading(value); }, 1);
        });
    }

    ngOnInit() {
        this.authService.checkSession().then(value => {
            if (value) {
                this.logged = value;
            } else {
                this.router.navigate(['login']);
            }
        });
        $(document).on('click', '[href="#"]', e => e.preventDefault());
    }

    changeLoading(value: boolean) {
        if (value !== this.loading) {
            this.loading = value;
        }
    }
}
