/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { HeaderComponent } from './header.component';

import { UserBlockService } from '../sidebar/userblock/user-block.service';
import { SettingsService } from '../../core/settings/settings.service';
import { MenuService } from '../../core/menu/menu.service';

describe('Component: Header', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [MenuService, UserBlockService, SettingsService]
        }).compileComponents();
    });

    it('should create an instance', async(inject([MenuService, UserBlockService, SettingsService], (menuService, userblockService, settingsService) => {
        let component = new HeaderComponent(menuService, userblockService, settingsService);
        expect(component).toBeTruthy();
    })));
});
