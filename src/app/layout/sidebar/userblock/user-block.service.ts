import {Injectable} from '@angular/core';

@Injectable()
export class UserBlockService {

    public visible: boolean;

    constructor() {
        this.visible = true;
    }

    getVisibility() {
        return this.visible;
    }

    setVisibility(stat = true) {
        this.visible = stat;
    }

    toggleVisibility() {
        this.visible = !this.visible;
    }
}
