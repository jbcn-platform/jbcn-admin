import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../core/auth/auth.service';
import {UserBlockService} from './user-block.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-user-block',
    templateUrl: './user-block.component.html',
    styleUrls: ['./user-block.component.scss']
})
export class UserBlockComponent implements OnInit {

    user: any;
    username: string;

    constructor(public userBlockService: UserBlockService,
                private router: Router,
                private authService: AuthService) {
        this.username = this.authService.getCurrentUsername();
        /* if (!this.username) {
            this.router.navigate(['login']);
        } */
        this.user = {
            picture: 'assets/img/user/01.jpg'
        };

    }

    ngOnInit() {
    }

    userBlockIsVisible() {
        return this.userBlockService.getVisibility();
    }

}
