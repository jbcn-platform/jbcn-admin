import {Component, OnInit} from '@angular/core';
import {loader} from '@shared/util/eventEmitters';
import {SearchResult, User} from '@shared/model/models';
import {UserService} from '@core/backend/user.service';

const swal = require('sweetalert2');

@Component({
    selector: 'app-user-list',
    templateUrl: 'user-list.component.html'
})
export class UserListComponent implements OnInit {

    term: string;
    page = 0;
    size = 20;

    searchResult: SearchResult<User>;

    constructor(private userService: UserService) {
        this.searchResult = new SearchResult<User>();
        this.searchResult.items = [];
    }

    ngOnInit() {
        this.search();
    }

    search() {
        this.page = 0;
        this.load();
    }

    nextPage() {
        this.page++;
        this.load();
    }

    previousPage() {
        this.page--;
        this.load();
    }

    loadPage(page: number) {
        this.page = page;
        this.load();
    }

    load() {
        loader.emit(true);
        this.userService.search(this.term, this.page, this.size, 'username', true)
            .then(searchResult => {
                this.searchResult = searchResult;
                loader.emit(false);
            });
    }

    deleteModal(item: User) {
        swal({
            title: 'Are you sure?',
            text: 'Do you want to delete user: ' + item.username + '?',
            type: 'warning',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Abort',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            console.log(result);
            if (result.value) {
                this.deleteUser(item);
            }
        });
    }

    deleteUser(user: User) {
        this.userService.delete(user).then(() => {
            console.log('Deleted');
            this.load();
        });
    }
}
