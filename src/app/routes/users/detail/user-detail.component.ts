import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '@shared/model/models';
import { UserService } from '@core/backend/user.service';

@Component({
    selector: 'app-user-detail',
    templateUrl: 'user-detail.component.html'
})
export class UserDetailComponent implements OnInit {

    user: User = new User()

    constructor(
        private route: ActivatedRoute,
        private userService: UserService
    ) { }

    ngOnInit() {
        let id = this.route.snapshot.params['id'];
        this.userService.get(id).then(user => {
            this.user = user;
        });
    }
}
