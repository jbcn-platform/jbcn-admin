import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { UserListComponent } from './list/user-list.component';
import { UserDetailComponent } from './detail/user-detail.component';
import { UserFormComponent } from './form/user-form.component';

const routes: Routes = [
    {
        path: '', children: [
            { path: 'list', component: UserListComponent },
            { path: 'form', component: UserFormComponent },
            { path: 'form/:id', component: UserFormComponent },
            { path: 'detail/:id', component: UserDetailComponent }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    exports: [],
    declarations: [
        UserListComponent,
        UserDetailComponent,
        UserFormComponent
    ],
    providers: [],
})
export class UsersModule { }
