import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorService } from '@core/error/error.service';
import { User } from '@shared/model/user';
import { UserService } from '@core/backend/user.service';

@Component({
    selector: 'app-user-form',
    templateUrl: 'user-form.component.html'
})
export class UserFormComponent implements OnInit {

    user: User = new User()
    selectedRole: string
    confirmPassword: string

    constructor(
        private route: ActivatedRoute,
        private errorService: ErrorService,
        private userService: UserService
    ) { }

    ngOnInit() {
        let id = this.route.snapshot.params['id'];
        if (id) {
            this.userService.get(id).then((user: User) => {
                console.log("Loaded user")
                console.log(user)
                this.user = user;
                if (user.roles && user.roles.length > 0) {
                    this.selectedRole = user.roles[0]
                }
            });
        }
    }

    save() {
        if (!this.user.username) {
            this.errorService.errorEvents.emit('error.username.mandatory');
            return;
        }

        if (!this.user.email) {
            this.errorService.errorEvents.emit('error.email.mandatory');
            return;
        }

        if (!this.user.roles == null || this.user.roles.length == 0) {
            this.errorService.errorEvents.emit('At least one role must be set');
            return;
        }
        if (!this.user._id) {
            if (!this.user.password) {
                this.errorService.errorEvents.emit('error.password.mandatory');
                return;
            }

            if (this.user.password !== this.confirmPassword) {
                this.errorService.errorEvents.emit('error.password.mismatch');
                return;
            }
        }

        // this.user.roles = [this.selectedRole]
        this.userService.save(this.user)
            .then(response => {
                this.errorService.successEvents.emit('operation.success');
                // this.router.navigate(['users', 'list']);
            });
    }
}
