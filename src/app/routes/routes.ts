import {LayoutComponent} from '../layout/layout.component';
import {LoginComponent} from './pages/login/login.component';

export const routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {path: '', redirectTo: 'home', pathMatch: 'full'},
            {path: 'home', loadChildren: './home/home.module#HomeModule'},
            {path: 'papers', loadChildren: './papers/papers.module#PapersModule'},
            {path: 'talks', loadChildren: './talks/talks.module#TalksModule'},
            {path: 'sponsors', loadChildren: './backoffice/sponsors/sponsors.module#SponsorsModule'},
            {path: 'url-redirects', loadChildren: './backoffice/url-redirects/redirects.module#RedirectsModule'},
            {path: 'job-offers', loadChildren: './backoffice/job-offers/jobOffers.module#JobOffersModule'},
            {path: 'tracks', loadChildren: './scheduling/tracks/tracks.module#TracksModule'},
            {path: 'rooms', loadChildren: './scheduling/rooms/rooms.module#RoomsModule'},
            {path: 'sessions', loadChildren: './scheduling/sessions/sessions.module#SessionsModule'},
            {path: 'attendees-votes', loadChildren: './attendees-votes/votes.module#VotesModule'},
            {path: 'badges', loadChildren: './badges/badges.module#BadgesModule'},
            {path: 'users', loadChildren: './users/users.module#UsersModule'},
        ]
    },

    // Not lazy-loaded routes
    {path: 'login', component: LoginComponent},

    // Not found
    {path: '**', redirectTo: 'home'}

];
