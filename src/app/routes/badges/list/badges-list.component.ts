import {Component, OnInit} from '@angular/core';
import {loader} from '@shared/util/eventEmitters';
import {SearchResult} from '@shared/model/models';
import {Badge, Sponsor} from '@shared/model/badge';
import {BadgesService} from '@core/backend/badges.service';
import {SponsorsService} from '@core/backend/sponsors.service';
import {Csv} from '@core/utils/csv';

const swal = require('sweetalert2');

@Component({
    selector: 'app-badges-list',
    templateUrl: 'badges-list.component.html',
    styleUrls: []
})
export class BadgesListComponent implements OnInit {

    searchResult = new SearchResult<Badge>();

    sponsors: Sponsor[];

    constructor(
        private service: BadgesService,
        private sponsorsService: SponsorsService
    ) {
    }

    ngOnInit() {
        this.load();
    }

    search() {
        this.service.getAll().then(allResults => {
            this.searchResult.items = allResults;
        });
    }

    saveAsCSV() {
        Csv.of(this.searchResult.items)
            .saveAsCSV('badges.csv',
                ['name', 'email', 'language', 'age', 'gender', 'company', 'city', 'country', 'proglang', 'jobtitle', 'details', '_id'],
                (it: Badge) => {
                    return [
                        it.attendee.name,
                        it.attendee.email,
                        it.attendee.language,
                        it.attendee.age,
                        it.attendee.gender,
                        it.attendee.company,
                        it.attendee.city,
                        it.attendee.country,
                        it.attendee.programmingLanguages,
                        it.attendee.jobTitle,
                        it.details,
                        it._id];
                });
    }

    load() {
        loader.emit(true);
        this.service.getAll().then(results => {
            this.searchResult.items = results;
            loader.emit(false);
            this.sponsorsService.getAll().then(response => {
                this.sponsors = response;
                this.searchResult.items.map(badge => {
                    badge.sponsor = this.sponsors.find(it => it.id === badge.sponsor.id);
                });
            });
        });
    }

    deleteModal(item: Badge) {
        swal({
            title: 'Are you sure?',
            text: 'Do you want to delete badge for: ' + item.attendee.name + '?',
            type: 'warning',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Abort',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                this.delete(item._id);
            }
        });
    }

    delete(id: string) {
        this.service.delete(id).then(() => {
            this.load();
        });
    }
}
