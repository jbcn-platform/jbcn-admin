import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Badge, Attendee, Sponsor } from '@shared/model/badge';
import { BadgesService } from '@core/backend/badges.service';
import { SponsorsService } from '@core/backend/sponsors.service';

@Component({
    selector: 'app-badges-form',
    templateUrl: 'badges-form.component.html',
    styleUrls: []
})
export class BadgesFormComponent implements OnInit {

    readonly urlContext = 'badges';

    item = new Badge();

    constructor(
        private route: ActivatedRoute,
        private service: BadgesService,
        private sponsorsService: SponsorsService
    ) { }

    ngOnInit() {
        this.item.attendee = new Attendee();
            this.item.sponsor = new Sponsor();
        const id = this.route.snapshot.params['id'];
        if (id) {
            this.service.get(id).then(it => {
                // console.log(it):
                this.item = it;
                this.sponsorsService.get(it.sponsor.id).then(sponsor => {
                    this.item.sponsor = sponsor;
                })
            });
        }
    }

}
