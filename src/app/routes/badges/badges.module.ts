import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BadgesListComponent } from './list/badges-list.component';
import { BadgesFormComponent } from './form/badges-form.component';
import { BadgesService } from '@core/backend/badges.service';
import { SponsorsService } from '@core/backend/sponsors.service';

const routes: Routes = [
    {
        path: '', children: [
            { path: 'list', component: BadgesListComponent },
            { path: 'form', component: BadgesFormComponent },
            { path: 'form/:id', component: BadgesFormComponent }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    exports: [],
    declarations: [
        BadgesListComponent,
        BadgesFormComponent
    ],
    providers: [
        BadgesService,
        SponsorsService
    ],
})
export class BadgesModule { }
