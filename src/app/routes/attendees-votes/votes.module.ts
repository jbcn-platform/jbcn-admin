import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { VotesListComponent } from './list/votes-list.component';
import { VotesFormComponent } from './form/votes-form.component';
import { VotesService } from '@core/backend/votes.service';
import { TalkService } from '@core/backend/talk.service';

const routes: Routes = [
    {
        path: '', children: [
            { path: 'list', component: VotesListComponent },
            { path: 'form', component: VotesFormComponent },
            { path: 'form/:id', component: VotesFormComponent }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    exports: [],
    declarations: [
        VotesListComponent,
        VotesFormComponent
    ],
    providers: [
        VotesService,
        TalkService
    ],
})
export class VotesModule { }
