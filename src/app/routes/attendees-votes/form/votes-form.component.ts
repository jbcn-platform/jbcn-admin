import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorService } from '@core/error/error.service';
import { Vote } from '@shared/model/vote';
import { VotesService } from '@core/backend/votes.service';

@Component({
    selector: 'app-votes-form',
    templateUrl: 'votes-form.component.html',
    styleUrls: []
})
export class VotesFormComponent implements OnInit {

    readonly urlContext = 'votes';

    item = new Vote();

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private errorService: ErrorService,
        private service: VotesService
    ) { }

    ngOnInit() {
        var id = this.route.snapshot.params['id']
        if (id) {
            this.service.get(id).then(it => {
                this.item = it as Vote
            });
        }
    }

    create() {
        if (this.isValidVote()) {
            console.log(this.item)
            this.service.create(this.item).then(response => {
                this.errorService.successEvents.emit('operation.success');
                this.router.navigate([this.urlContext, 'list']);
            });
        }
    }

    update() {
        if (this.isValidVote()) {
            this.service.update(this.item).then(response => {
                this.errorService.successEvents.emit('operation.success');
                this.router.navigate([this.urlContext, 'list']);
            });
        }
    }

    private isValidVote() {
        if (!this.item.talkId) {
            this.errorService.errorEvents.emit('error.talk.mandatory');
            return false;
        }
        if (!this.item.userEmail) {
            this.errorService.errorEvents.emit('error.userEmail.mandatory');
            return false;
        }
        if (!this.item.value) {
            this.errorService.errorEvents.emit('error.value.mandatory');
            return false;
        }
        return true;
    }

}
