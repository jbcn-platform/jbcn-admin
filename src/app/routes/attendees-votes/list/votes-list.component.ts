import { Component, OnInit } from '@angular/core';
import { loader } from '@shared/util/eventEmitters';
import { SearchResult, Talk, ISearchParams } from '@shared/model/models';
import { Vote } from '@shared/model/vote';
import { VotesService } from '@core/backend/votes.service';
import { TalkService } from '@core/backend/talk.service';
import { saveAs } from 'file-saver';

const swal = require('sweetalert2');

@Component({
    selector: 'app-votes-list',
    templateUrl: 'votes-list.component.html',
    styleUrls: []
})
export class VotesListComponent implements OnInit {

    searchResult = new SearchResult<Vote>();

    talks: Talk[];
    workshops: Talk[];
    talkId: string;
    talksCache: Map<String, Talk>;

    constructor(
        private service: VotesService,
        private talksService: TalkService) {
    }

    ngOnInit() {
        this.load();
    }

    search() {
        this.service.getAll().then(allResults => {
            this.searchResult.items = allResults
                .filter(it => this.talkId.length === 0 || it.talkId === this.talkId);
            this.fillTalkInfo(this.searchResult.items);
        });
    }

    saveAsCSV() {
        let buffer = "_id;talkId;talk;speaker_1;speaker_2;userEmail;value;delivery;other\n";
        this.searchResult.items
            .map(it => buffer += `${it._id};${it.talkId};${it.talk.title};${it.talk.speakers[0].fullName};${it.talk.speakers.length == 2 ? it.talk.speakers[1].fullName : ''};${it.userEmail};${it.value};${it.delivery};${it.other}\n`)
        var blob = new Blob([buffer], { type: "text/csv;charset=utf-8" });
        saveAs(blob, "votes.csv");
    }

    load() {
        loader.emit(true)
        this.service.getAll().then(allResults => {
            this.searchResult.items = allResults
            this.talksService.search({
                'term': null,
                'sort': 'title',
                'asc': null,
                'page': 0,
                'size': 500,
                'filters': [],
                'fields': ['title', 'type', 'speakers']
            } as ISearchParams).then(talksSearch => {
                this.talksCache = new Map<String, Talk>()
                talksSearch.items
                    .forEach(it => this.talksCache.set(it._id, it as Talk))
                this.searchResult.items
                    .forEach(it => it.talk = this.talksCache.get(it.talkId))

                this.talks = []
                this.workshops = []
                let talkIdsWithVotes = allResults.map(it => it['talkId'])
                talksSearch.items
                    .filter(it => talkIdsWithVotes.indexOf(it['_id']) >= 0)
                    .forEach(it => {
                        if (it['type'] == 'talk')
                            this.talks.push(it)
                        else
                            this.workshops.push(it)
                    })
                loader.emit(false)
            });
        });
    }

    private fillTalkInfo(votes: Vote[]) {

    }

    deleteModal(item: Vote) {
        swal({
            title: 'Are you sure?',
            text: 'Do you want to delete vote: ' + item._id + '?',
            type: 'warning',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Abort',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                this.delete(item._id)
            }
        });
    }

    delete(id: string) {
        this.service.delete(id).then(result => {
            this.load()
        });
    }

}
