import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@shared/shared.module';

import {PaperListComponent} from './list/paper-list.component';
import {PaperDetailComponent} from './detail/paper-detail.component';
import {PaperFormComponent} from './form/paper-form.component';
import {PaperStreamComponent} from './vote-stream/paper-stream.component';
import {PaperVotesDashboardComponent} from './vote-dashboard/dashboard.component';

const routes: Routes = [
    {
        path: '', children: [
            {path: 'list', component: PaperListComponent},
            {path: 'detail/:id', component: PaperDetailComponent},
            {path: 'form', component: PaperFormComponent},
            {path: 'form/:id', component: PaperFormComponent},
            {path: 'votes/dashboard', component: PaperVotesDashboardComponent},
            {path: 'votes/inbox', component: PaperStreamComponent}
        ]
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FormsModule,
        CommonModule
    ],
    exports: [],
    declarations: [
        PaperListComponent,
        PaperDetailComponent,
        PaperFormComponent,
        PaperVotesDashboardComponent,
        PaperStreamComponent
    ],
    providers: [],
})
export class PapersModule {
}
