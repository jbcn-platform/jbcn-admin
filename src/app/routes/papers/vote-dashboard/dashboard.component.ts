import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '@core/auth/auth.service';
import {PaperService} from '@core/backend/paper.service';
import {SearchFilter} from '@shared/model/models';


@Component({
    selector: 'papervotes-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: []
})
export class PaperVotesDashboardComponent implements OnInit {

    talks: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    talksTotal = 0;
    workshops: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    workshopsTotal = 0;

    Math = Math;

    constructor(
        private authService: AuthService,
        private paperService: PaperService,
        private router: Router) {
    }

    ngOnInit() {
        let username = '';
        let url = this.router.url;
        if (url.endsWith('votes/dashboard')) {
            username = this.authService.getCurrentUsername();
        } else {
            username = url.substring(url.lastIndexOf('/'));
            console.log(username);
        }
        for (let i = 0; i < 10; i++) {
            this.countPapers(username, 'talk', i);
            this.countPapers(username, 'workshop', i);
        }
    }

    private countPapers(username: string, type: string, vote: number) {
        this.paperService.search({
            all: [
                SearchFilter.equals('type', type),
                SearchFilter.vote(username, vote)
            ]
        }, 0, 1, '_id', true)
            .then(result => {
                if (type === 'talk') {
                    this.talks[vote] = result.total;
                    this.talksTotal = this.talks.reduce((a, b) => a + b, 0);
                }
                if (type === 'workshop') {
                    this.workshops[vote] = result.total;
                    this.workshopsTotal = this.workshops.reduce((a, b) => a + b, 0);
                }
            });
    }
}
