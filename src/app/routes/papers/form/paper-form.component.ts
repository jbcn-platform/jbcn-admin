import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Paper, Sender} from '@shared/model/models';
import {PaperService} from '@core/backend/paper.service';

const swal = require('sweetalert2');

@Component({
    selector: 'app-paper-form',
    templateUrl: 'paper-form.component.html'
})

export class PaperFormComponent implements OnInit {

    // early init or else html fails
    paper = new Paper();
    languages: string;
    tags: string;

    editions: number[] = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private paperService: PaperService
    ) {
    }

    ngOnInit() {
        const id = this.route.snapshot.params['id'];
        if (id) {
            this.paperService.get(id).then(result => {
                this.paper = result;
                this.tags = this.paperService.stringArrayAsText(this.paper.tags);
                this.languages = this.paperService.stringArrayAsText(this.paper.languages);
            });
        } else {
            this.addSender();
        }
        const currentYear = new Date().getFullYear();
        for (let i = currentYear; i >= 2018; i--) {
            this.editions.push(i);
        }
        this.paper.edition = String(currentYear);
    }

    addSender() {
        this.paper.senders.push(new Sender());
    }

    removeSender(position: number) {
        this.paper.senders.splice(position, 1);
    }

    cancelModal(item: Paper) {
        swal({
            title: 'Are you sure you want to cancel paper: ' + item.title + '?',
            text: 'Note it won\'t delete it, just update it\'s status',
            type: 'warning',
            confirmButtonText: 'Yes, cancel it!',
            cancelButtonText: 'Abort',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                let paper = new Paper();
                paper._id = item._id;
                paper.state = 'canceled';
                this.paperService.update(paper)
                    .then(() => {
                        this.paper.state = 'canceled';
                    });
            }
        });
    }

    save() {
        this.paper.tags = this.tags.split(',');
        this.paper.languages = this.languages.split(',');
        this.paperService.save(this.paper).then(() => {
            this.router.navigate(['/papers/list']);
        });
    }
}
