import {loader} from '@shared/util/eventEmitters';
import {AuthService} from '@core/auth/auth.service';
import {Role} from '@core/auth/role';
import {Paper, PaperNote} from '@shared/model/models';
import {PaperService} from '@core/backend/paper.service';

const swal = require('sweetalert2');

export class NotesHandler {

    readonly username = this.authService.getCurrentUsername();

    paper: Paper = new Paper();
    notes: PaperNote[] = [];
    newNoteText: string;
    feedbackText: string;

    constructor(
        protected authService: AuthService,
        protected paperService: PaperService,
    ) {
    }

    loadNotes(emit: boolean) {
        if (this.authService.hasRole(Role.ADMIN, Role.VOTER)) {
            this.paperService.getNotes(this.paper._id).then(notes => {
                this.notes = notes;
                if (emit) {
                    loader.emit(false);
                }
            });
        } else {
            loader.emit(false);
        }
    }

    addNote() {
        if (this.newNoteText) {
            const newNote = {paperId: this.paper._id, text: this.newNoteText};
            loader.emit(true);
            this.paperService.addNote(newNote as PaperNote).then(() => {
                this.newNoteText = '';
                this.loadNotes(true);
            }, error => {
                loader.emit(false);
            });
        }
    }

    editNote(note: PaperNote) {
        loader.emit(true);
        this.paperService.updateNote({id: note.id, text: note.text} as PaperNote).then(() => {
            loader.emit(false);
            this.loadNotes(false);
        });
    }

    deleteNote(note: PaperNote) {
        const msg = (note.text.length > 10)
            ? 'Are you sure you want to delete the note starting with: ' + note.text.substr(0, 10) + '...?'
            : 'Are you sure you want to delete the note with text: ' + note.text.substr(0, 10) + '?';
        swal({
            title: msg,
            // text: 'This will delete all paper\'s related data like sender, comments, votes, etc.',
            type: 'warning',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Abort',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                this.paperService.deleteNote({id: note.id} as PaperNote).then(() => {
                    this.loadNotes(true);
                });
            }
        });
    }
}
