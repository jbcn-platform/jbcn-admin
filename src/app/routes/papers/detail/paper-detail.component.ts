import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {loader} from '@shared/util/eventEmitters';
import {Paper} from '@shared/model/models';
import {AuthService} from '@core/auth/auth.service';
import {PaperService} from '@core/backend/paper.service';
import {PaperStreamService} from '@core/backend/paper-stream.service';
import {NotesHandler} from '../notes-handler';
import {MailService} from '@core/backend/mail.service';
import {environment} from '@environments/environment';

const swal = require('sweetalert2');

@Component({
    selector: 'app-paper-detail',
    templateUrl: 'paper-detail.component.html',
    styles: ['paper-detail.component.scss']
})
export class PaperDetailComponent extends NotesHandler implements OnInit {

    userPermissions: string[];
    yourVote: number;
    feedBackText: string;

    readonly publicView = `${environment.publicView}`;

    constructor(
        authService: AuthService,
        paperService: PaperService,
        private route: ActivatedRoute,
        private router: Router,
        private paperStreamService: PaperStreamService,
        private mailService: MailService
    ) {
        super(authService, paperService);
    }

    ngOnInit() {
        const id = this.route.snapshot.params['id'];
        if (id) {
            this.loadPaper(id);
        }
        this.userPermissions = this.authService.getCurrentUserPermissions();
    }

    loadPaper(id: string) {
        loader.emit(true);
        this.paperService.get(id).then(result => {
            this.paper = result;
            console.log(result);
            if (this.paper.votes) {
                const myVote = this.paper.votes.filter(v => v.username === this.username);
                this.yourVote = myVote.length > 0 ? myVote[0].vote : 0;
            }

            if (result.feedback) {
                this.feedBackText = result.feedback.text;
            }
            this.loadNotes(true);
        });
    }

    // NOTE: even is set to number, a string is received
    vote(value: string) {
        const paperId = this.paper._id;
        this.paperStreamService.vote(paperId, +value).then(() => {
            this.loadPaper(paperId);
        });
    }

    togglePaperSponsor() {
        const updateRequest = {_id: this.paper._id, sponsor: this.paper.sponsor};
        this.paperService.update(updateRequest as Paper);
    }

    deleteModal(item: Paper) {
        swal({
            title: 'Are you sure you want to delete paper: ' + item.title + '?',
            text: 'This will delete all paper\'s related data like sender, comments, votes, etc.',
            type: 'warning',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Cancel',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                return this.deletePaper(item._id);
            }
        });
    }

    deletePaper(id: string) {
        this.paperService.delete(id)
            .then(() => {
                console.log('deleted paper: ' + id);
                this.router.navigate(['/papers/list']);
            });
    }

    hasCurrentUserVote(): boolean {
        return this.yourVote !== 0;
    }

    addFeedback() {
        this.paperService.addFeedback(this.paper._id, this.feedBackText)
            .then(() => {
                this.loadPaper(this.paper._id);
            });
    }

    updateFeedback() {
        this.paperService.updateFeedback(this.paper._id, this.feedBackText)
            .then(() => {
                this.loadPaper(this.paper._id);
            });
    }

    sendEmailModal(item: Paper) {
        swal({
            title: 'Email with feedback will be sent to speaker(s)?',
            text: 'There is no rollback from a sent email',
            type: 'warning',
            confirmButtonText: 'Yes, sent it!',
            cancelButtonText: 'Cancel',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                console.log(`Sending ${this.paper._id}`)
                this.mailService.sendFeedback(item._id);
            }
        });
    }
}
