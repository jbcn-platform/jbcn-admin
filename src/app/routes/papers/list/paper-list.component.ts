import {Component, OnInit} from '@angular/core';
import {Company, Paper, SearchConditions, SearchFilter, SearchResult, Sender} from '@shared/model/models';
import {loader} from '@shared/util/eventEmitters';
import {SearchComponent} from '@shared/util/search.component';
import {PaperService} from '@core/backend/paper.service';
import {UserService} from '@core/backend/user.service';
import {CompanyService} from '@core/backend/company.service';
import {Csv} from '@core/utils/csv';
import {AuthService} from '@core/auth/auth.service';

const swal = require('sweetalert2');

@Component({
    selector: 'app-paper-list',
    templateUrl: 'paper-list.component.html',
    styleUrls: ['./paper-list.component.css']
})
export class PaperListComponent extends SearchComponent implements OnInit {

    readonly username = this.authService.getCurrentUsername();

    // global search
    term: string;
    title = true;
    abstract = true;
    comments = true;
    tags = true;
    senderName = true;
    senderBio = true;
    senderCompany = true;

    // paper fields
    state = '';
    type = '';
    sponsorPapers = false;
    // sender fields
    excludeSpeakers = false;
    company = '';

    voter = '';
    voteValue: number = null;

    page = 0;
    size = 20;
    pages = [];
    sortColumn = 'createdDate';
    sortAsc = false;

    companies: Company[];
    searchResult = new SearchResult<Paper>();
    selectedToDelete: Paper;
    showDelete = false;

    voters = {};

    constructor(
        private paperService: PaperService,
        private companyService: CompanyService,
        private userService: UserService,
        private authService: AuthService,
    ) {
        super();
    }

    ngOnInit() {
        this.loadParams();
        this.search();
        this.companyService.getAll('paper').then(result => {
            this.companies = result;
        });
    }

    isShown(paper: Paper) {
        return paper.votes && (paper.votes.length >= this.searchResult.votersCount) && (paper.state !== 'accepted');
    }

    loadParams() {
        this.userService.getVoters().then(voters => {
            voters.forEach((voter) => {
                this.voters[voter.username] = voter.username;
            });
        });
        const params = this.getStoredParams('papers');
        if (params) {
            this.term = params['term'];
            this.title = params['title'];
            this.abstract = params['abstract'];
            this.comments = params['comments'];
            this.tags = params['tags'];
            this.senderName = params['senderName'];
            this.senderBio = params['senderBio'];

            this.state = params['state'];
            this.type = params['type'];
            this.sponsorPapers = params['sponsorPapers'] === undefined ? false : params['sponsorPapers'];
            this.excludeSpeakers = params['excludeSpeakers'] === undefined ? false : params['excludeSpeakers'];
            this.company = params['company'];

            this.page = params['page'];
            this.size = params['size'];
            this.sortColumn = params['sortColumn'];
            this.sortAsc = params['sortAsc'];
            this.voter = params['voter'] === undefined ? '' : params['voter'];
            this.voteValue = params['voteValue'] === undefined ? null : params['voteValue'];
        }
    }

    storeParams() {
        const params = {
            'term': this.term,
            'title': this.title,
            'abstract': this.abstract,
            'comments': this.comments,
            'tags': this.tags,
            'senderName': this.senderName,
            'senderBio': this.senderBio,

            'state': this.state,
            'type': this.type,
            'sponsorPapers': this.sponsorPapers,
            'excludeSpeakers': this.excludeSpeakers,
            'company': this.company,

            'page': this.page,
            'size': this.size,
            'sortColumn': this.sortColumn,
            'sortAsc': this.sortAsc,
            'voter': this.voter,
            'voteValue': this.voteValue
        };
        this.putStoredParams('papers', params);
    }

    // workaround to ensure number type
    setVoteValue(value) {
        this.voteValue = parseInt(value);
    }

    voterNames(): Array<string> {
        const values = [];
        for (const k in this.voters) {
            values.push(k);
        }
        return values.sort();
    }

    search() {
        this.page = 0;
        this.load();
    }

    nextPage() {
        if (this.page < (this.searchResult.totalPages - 1)) {
            this.page++;
            this.load();
        }
    }

    goToPage(page) {
        this.page = page;
        this.load();
    }

    previousPage() {
        if (this.page > 0) {
            this.page--;
            this.load();
        }
    }

    loadPage(page: number) {
        this.page = page;
        this.load();
    }

    setSort(sort: string) {
        if (sort === this.sortColumn) {
            this.sortAsc = !this.sortAsc;
        } else {
            this.sortColumn = sort;
            this.sortAsc = true;
        }
        this.load();
    }

    load() {
        loader.emit(true);

        this.paperService.search(
            this.buildSearchConditions(),
            this.page,
            this.size,
            this.sortColumn,
            this.sortAsc
        ).then(searchResult => {
            loader.emit(false);
            this.storeParams();

            this.searchResult = searchResult;
            this.searchResult.votersCount = searchResult.votersCount;
            this.searchResult.page = this.page;
            this.searchResult.totalPages = Math.floor(searchResult.total / this.size);
            if (searchResult.total % this.size !== 0) {
                this.searchResult.totalPages = this.searchResult.totalPages + 1;
            }
            this.pages = [];
            for (let i = 0; i < searchResult.totalPages; i++) {
                this.pages.push(i);
            }
        });
    }

    private buildSearchConditions() {
        const allFilters = [];
        // paper fields
        if (this.state && this.state !== '') {
            allFilters.push(SearchFilter.equals('state', this.state));
        }
        if (this.type && this.type !== '') {
            allFilters.push(SearchFilter.equals('type', this.type));
        }
        if (this.sponsorPapers) {
            allFilters.push(SearchFilter.equals('sponsor', true));
        }
        // sender fields
        if (this.excludeSpeakers) {
            allFilters.push(SearchFilter.equals('not-published-speakers', true));
        }
        if (this.company && this.company !== '') {
            allFilters.push(SearchFilter.equals('sender.company', this.company));
        }
        // voting
        if (this.voter && (this.voter !== '' || (this.voteValue != null && !isNaN(this.voteValue)))) {
            allFilters.push(SearchFilter.vote(this.voter, this.voteValue));
        }
        /* _ */
        const anyFilters = [];
        if (this.term && this.term !== '') {
            if (this.title === true) {
                anyFilters.push(SearchFilter.contains('title', this.term));
            }
            if (this.abstract === true) {
                anyFilters.push(SearchFilter.contains('abstract', this.term));
            }
            if (this.comments === true) {
                anyFilters.push(SearchFilter.contains('comments', this.term));
            }
            if (this.tags === true) {
                anyFilters.push(SearchFilter.contains('tags', this.term));
            }
            if (this.senderName === true) {
                anyFilters.push(SearchFilter.contains('sender.fullName', this.term));
            }
            if (this.senderBio === true) {
                anyFilters.push(SearchFilter.contains('sender.biography', this.term));
            }
            if (this.senderBio === true) {
                anyFilters.push(SearchFilter.contains('sender.company', this.term));
            }
        }

        const conditions: SearchConditions = {
            all: allFilters,
            any: anyFilters
        };
        return conditions;
    }

    setDelete(item: Paper) {
        this.selectedToDelete = item;
    }

    deleteModal(item: Paper) {
        swal({
            title: 'Are you sure?',
            text: 'Do you want to delete paper' + item.title + '? This will delete all paper\'s related data like comments, votes, etc...',
            type: 'warning',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Abort',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                this.deletePaper(item);
            }
        });
    }

    needsMoney(senders: Array<Sender>): boolean {
        let result = false;
        for (const sender of senders) {
            if (sender.travelCost) {
                result = true;
                break;
            }
        }
        return result;
    }

    isCanceled(state: string): boolean {
        return state === 'canceled';
    }

    approveModal(item: Paper) {
        swal({
            title: 'Are you sure?',
            text: 'Do you want to approve paper "' + item.title + '" ?',
            type: 'warning',
            confirmButtonText: 'Yes, approve it!',
            cancelButtonText: 'Cancel',
            showCancelButton: true
        }).then((result) => {
            if (result.value) {
                swal({
                    title: 'Select a name of Juger as a person of contact',
                    input: 'select',
                    inputOptions: this.voters,
                    confirmButtonText: 'Approve this person',
                    cancelButtonText: 'Cancel',
                    showCancelButton: true
                }).then((name) => {
                        if (name.value !== null && name.value !== undefined) {
                            this.approvePaper(item, name.value);
                        }
                    }
                );
            }
        });
    }

    approvePaper(item: Paper, name: string) {
        this.paperService.approve(item, name).then((result) => {
            if (result) {
                this.load();
            }
        });
    }

    deletePaper(item: Paper) {
        this.paperService.delete(item._id).then((result) => {
            if (result) {
                this.selectedToDelete = null;
                this.load();
            }
        });
    }

    showDeleteButtons() {
        this.showDelete = true;
    }

    formatSendersNames(item: Paper): string {
        const senders = item.senders;
        if (senders.length === 0) {
            return '';
        }
        if (senders.length === 1) {
            return senders[0].fullName;
        } else {
            return senders.map(s => s.fullName).join(', ');
        }
    }

    getVotesNum(paper: Paper) {
        if (!paper.votes) {
            return 0;
        } else {
            return paper.votes.length;
        }
    }

    saveAsCSV() {
        this.paperService
            .search(this.buildSearchConditions(), 0, 1000, 'senders.fullName', true)
            .then(result => {
                Csv.of(result.items)
                    .saveAsCSV('papers.csv',
                        [
                            'type', 'title', 'state',
                            'First speaker name', 'First speaker email', 'First speaker twitter',
                            'Second speaker name', 'Second speaker email', 'Second speaker twitter',
                            '_id'
                        ],
                        (value: Paper) => {
                            return [
                                value.type,
                                value.title,
                                value.state,
                                value.senders[0].fullName,
                                value.senders[0].email,
                                value.senders[0].twitter,
                                value.senders.length === 2 ? value.senders[1].fullName : '',
                                value.senders.length === 2 ? value.senders[1].email : '',
                                value.senders.length === 2 ? value.senders[1].twitter : '',
                                value._id
                            ];
                        });
            });
    }

    hasCurrentUserVote(item: Paper): boolean {
        return 'votes' in item && item.votes.some(v => v.username === this.username);
    }
}
