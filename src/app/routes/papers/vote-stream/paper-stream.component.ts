import {Component, OnInit} from '@angular/core';
import {loader} from '@shared/util/eventEmitters';
import {AuthService} from '@core/auth/auth.service';
import {ErrorService} from '@core/error/error.service';
import {Paper, SearchFilter, Sender} from '@shared/model/models';
import {PaperService} from '@core/backend/paper.service';
import {PaperStreamService} from '@core/backend/paper-stream.service';
import {NotesHandler} from '../notes-handler';

@Component({
    selector: 'app-stream-paper',
    templateUrl: 'paper-stream.component.html',
    styleUrls: ['./paper-stream.component.css']
})
export class PaperStreamComponent extends NotesHandler implements OnInit {

    stream: Paper[] = [];
    currentPosition: number = 0;

    type = 'all';
    sortSelect = 'createdDate_asc';
    sortField = 'createdDate';
    asc = true;

    vote = 0;
    sender: Sender;
    senderPapers: Paper[] = [];

    speakersPapers: Map<string, Paper[]> = new Map();


    constructor(
        protected authService: AuthService,
        protected paperService: PaperService,
        private streamService: PaperStreamService,
        private errorService: ErrorService
    ) {
        super(authService, paperService);
    }

    ngOnInit() {
        this.resetStream();
    }


    private mapBoolean(value: string): boolean {
        return value === 'asc';
    }

    resetStream() {
        loader.emit(true);
        this.streamService.getPaperStream()
            .then(papers => {
                let strings = this.sortSelect.split('_');
                this.sortField = strings[0];
                this.asc = this.mapBoolean(strings[1]);

                this.stream = papers;
                if (this.stream.length > 0) {
                    this.sortStream();
                    this.initializeAllPaperInformation();
                } else {
                    loader.emit(false);
                }
            });
    }

    private initializeAllPaperInformation() {
        this.loadPaperAndNotes(this.currentPosition, true);
        this.loadOtherPapers(this.paper.senders);
    }

    private sortStream() {
        this.stream = this.stream.sort(this.genericComparator);
    }

    refreshStream() {
        // TODO if there are non ahead, should go backwards
        if (this.type != 'all' && this.stream[this.currentPosition].type != this.type) {
            if (this.hasNext()) {
                this.next();
            } else {
                this.back();
            }
        }
    }

    selectSender(sender: Sender) {
        this.sender = sender;
        this.senderPapers = this.speakersPapers.get(sender.fullName);
    }

    getTalksCount(): number {
        return this.stream.reduce((acc, p) => p.type === 'talk' ? ++acc : acc, 0);
    }

    getWorkshopsCount(): number {
        return this.stream.length - this.getTalksCount();
    }

    next() {
        if (this.hasNext()) {
            loader.emit(true);
            this.currentPosition++;
            if (this.type != 'all') {
                while (this.stream[this.currentPosition].type != this.type) {
                    this.currentPosition++;
                }
            }
            this.initializeAllPaperInformation();
        }
    }

    back() {
        if (this.hasPrevious()) {
            loader.emit(true);
            this.currentPosition--;
            if (this.type != 'all') {
                while (this.stream[this.currentPosition].type != this.type) {
                    this.currentPosition--;
                }
            }
            this.initializeAllPaperInformation();
        }
    }

    private hasNext() {
        for (let i = this.currentPosition + 1; i < this.stream.length; i++) {
            if (this.type == 'all' || this.stream[i].type == this.type) {
                return true;
            }
        }
        return false;
    }

    private hasPrevious() {
        for (let i = this.currentPosition - 1; i >= 0; i--) {
            if (this.type == 'all' || this.stream[i].type == this.type) {
                return true;
            }
        }
        return false;
    }

    doVote() {
        loader.emit(true);
        this.streamService.vote(this.paper._id, this.vote).then(result => {
            loader.emit(false);
            if (result) {
                this.errorService.successEvents.emit('Operation Success');
            }
            this.removePaper(this.currentPosition);
            if (this.stream.length > 0) {
                this.vote = 0;
                this.initializeAllPaperInformation();
            } else {
                this.paper = null;
            }
        }, () => {
            loader.emit(false);
        });
    }

    private loadPaperAndNotes(paperPosition: number, emit: boolean) {
        this.paper = this.stream[paperPosition];
        if (this.paper) {
            this.loadNotes(emit);
            if (this.paper.votes) {
                let userVote = this.paper.votes.find(p => p.username === this.username);
                if (userVote) {
                    this.vote = userVote.vote;
                }
            }
        } else {
            loader.emit(false);
        }
    }

    private loadOtherPapers(senders: Array<Sender>) {
        senders.forEach(sender => {
            let conditions = {
                all: [SearchFilter.equals('sender.fullName', sender.fullName)]
            };
            this.paperService.search(conditions, 0, 20, 'dateCreated', false)
                .then(result => {
                    let values = result.items.filter(it => it._id != this.paper._id);
                    this.speakersPapers.set(sender.fullName, values);
                });
        });
    }

    private removePaper(paperPosition: number) {
        this.stream.splice(paperPosition, 1);
        if (this.currentPosition == this.stream.length) {
            this.currentPosition--;
        }
    }

    getGoogleQuery() {
        if (this.paper && this.paper.senders && this.paper.senders.length > 0) {
            return encodeURIComponent(this.paper.title + ' ' + this.paper.senders[0].fullName);
        } else {
            return '';
        }
    }

    private genericComparator = (a: Paper, b: Paper) => {
        if (a[this.sortField] == null) {
            return (b[this.sortField] == null) ? (this.asc ? 0 : 1) : (this.asc ? 1 : 0);
        }
        if (b[this.sortField] == null) {
            return this.asc ? 1 : -1;
        }
        if (a[this.sortField] < b[this.sortField]) {
            return this.asc ? -1 : 1;
        }
        if (a[this.sortField] > b[this.sortField]) {
            return this.asc ? 1 : -1;
        }
        return 0;
    };
}
