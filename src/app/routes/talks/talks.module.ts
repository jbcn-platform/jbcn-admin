import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';

import { TalkListComponent } from './list/talk-list.component';
import { TalkDetailComponent } from './detail/talk-detail.component';
import { TalkFormComponent } from './form/talk-form.component';
import { RoomsService } from '@core/backend/rooms.service';
import { SessionsService } from '@core/backend/sessions.service';

const routes: Routes = [
    {
        path: '', children: [
            { path: 'list', component: TalkListComponent },
            { path: 'detail/:id', component: TalkDetailComponent },
            { path: 'form/:id', component: TalkFormComponent },
        ]
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FormsModule,
        CommonModule
    ],
    exports: [],
    declarations: [
        TalkListComponent,
        TalkDetailComponent,
        TalkFormComponent
    ],
    providers: [
        RoomsService,
        SessionsService
    ],
})

export class TalksModule { }
