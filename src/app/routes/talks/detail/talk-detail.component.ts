import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {loader} from '@shared/util/eventEmitters';
import {Session, Talk} from '@shared/model/models';
import {TalkService} from '@core/backend/talk.service';
import {SessionsService} from '@core/backend/sessions.service';
import {RoomsService} from '@core/backend/rooms.service';
import {environment} from '@environments/environment';

@Component({
    selector: 'app-talk-detail',
    templateUrl: 'talk-detail.component.html'
})
export class TalkDetailComponent implements OnInit {

    talk: Talk;
    sessions: Session[];

    readonly publicView = `${environment.publicView}`;

    constructor(
        private route: ActivatedRoute,
        private talkService: TalkService,
        private sessionsService: SessionsService,
        private roomsService: RoomsService
    ) {
    }

    ngOnInit() {
        this.talk = new Talk();
        const id = this.route.snapshot.params['id'];
        loader.emit(true);
        if (id) {
            this.talkService.get(id).then(talk => {
                this.talk = talk;
                loader.emit(false);
                this.setSessionInfo(this.talk);
            });
        }
        this.sessionsService.getAll()
            .then(result => this.sessions = result);
    }

    updateSession(value: string) {
        const update = {_id: this.talk._id, sessionId: value};
        this.talkService.update(update as Talk).then(result => {
            this.setSessionInfo(this.talk);
        });
    }

    private setSessionInfo(talk: Talk) {
        if (talk.sessionId) {
            this.sessionsService.get(talk.sessionId)
                .then(result => {
                    talk.session = result;
                    if (result.roomId) {
                        this.roomsService.get(result.roomId)
                            .then(room => talk.session.room = room);
                    }
                });
        }
    }
}
