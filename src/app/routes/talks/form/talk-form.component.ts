import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {loader} from '@shared/util/eventEmitters';
import {Speaker, Talk} from '@shared/model/models';
import {TalkService} from '@core/backend/talk.service';

@Component({
    selector: 'app-talk-form',
    templateUrl: 'talk-form.component.html'
})
export class TalkFormComponent implements OnInit {

    talk: Talk;
    languages: string;
    tags: string;

    constructor(
        private route: ActivatedRoute,
        private talkService: TalkService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.talk = new Talk();
        const id = this.route.snapshot.params['id'];
        loader.emit(true);
        if (id) {
            this.talkService.get(id).then(talk => {
                loader.emit(false);
                this.talk = talk;
                this.tags = this.talkService.stringArrayAsText(this.talk.tags);
                this.languages = this.talkService.stringArrayAsText(this.talk.languages);
                console.log(this.talk);
            });
        }
    }

    save() {
        loader.emit(true);
        this.talk.tags = this.tags.split(',');
        this.talk.languages = this.languages.split(',');
        this.talkService.update(this.talk).then(() => {
            loader.emit(false);
            this.router.navigate([`/talks/detail/${this.talk._id}`]);
        });
    }

    addSpeaker() {
        this.talk.speakers.push(new Speaker());
    }

    removeSpeaker(position: number) {
        this.talk.speakers.splice(position, 1);
    }
}
