import {Component, OnInit} from '@angular/core';
import {loader} from '@shared/util/eventEmitters';
import {SearchComponent} from '@shared/util/search.component';
import {Company, SearchFilter, SearchResult, Talk} from '@shared/model/models';
import {ErrorService} from '@core/error/error.service';
import {TalkService} from '@core/backend/talk.service';
import {UserService} from '@core/backend/user.service';
import {CompanyService} from '@core/backend/company.service';
import {environment} from '@environments/environment';
import {PapersSearchParams} from '@shared/model/searchParams';
import {Csv} from '@core/utils/csv';

const swal = require('sweetalert2');

@Component({
    selector: 'app-talk-list',
    templateUrl: 'talk-list.component.html',
    styleUrls: ['./talk-list.component.css']
})
export class TalkListComponent extends SearchComponent implements OnInit {

    // talk fields
    term: string;
    state = '';
    published = 'all';
    // sender fields
    company = '';
    excludeCanceled = true;

    page = 0;
    size = 20;
    pages = [];
    sortColumn = 'createdDate';
    sortAsc = false;

    companies: Company[];
    searchResult = new SearchResult<Talk>();
    inputOptions = {};
    states = ['unconfirmed', 'confirmed', 'scheduled', 'canceled'];

    formatAsHtml = true;
    format = 'html';

    readonly publicApiUrl = `${environment.apiPublicBase}`;

    constructor(
        private talkService: TalkService,
        private companyService: CompanyService,
        private errorService: ErrorService,
        private userService: UserService
    ) {
        super();
    }

    ngOnInit() {
        this.loadParams();
        this.search();
        this.companyService.getAll('talk').then(result => {
            this.companies = result;
        });
    }

    loadParams() {
        const params = this.getStoredParams('talks');
        if (params) {
            this.term = params['term'];
            this.state = params['state'];
            this.published = params['published'];
            this.company = params['company'];

            this.page = params['page'];
            this.size = params['size'];
            this.sortColumn = params['sortColumn'];
            this.sortAsc = params['sortAsc'];
        }

        this.userService.getVoters().then(voters => {
            voters.forEach((voter) => {
                this.inputOptions[voter.username] = voter.username;
            });
        });

    }

    storeParams() {
        const params = {
            'term': this.term,
            'state': this.state,
            'published': this.published,
            'company': this.company,
            'page': this.page,
            'size': this.size,
            'sortColumn': this.sortColumn,
            'sortAsc': this.sortAsc,
        };
        this.putStoredParams('talks', params);
    }

    search() {
        this.page = 0;
        this.load();
    }

    nextPage() {
        if (this.page < (this.searchResult.totalPages - 1)) {
            this.page++;
            this.load();
        }
    }

    previousPage() {
        if (this.page > 0) {
            this.page--;
            this.load();
        }
    }

    loadPage(page: number) {
        this.page = page;
        this.load();
    }

    setSort(sort: string) {
        if (sort === this.sortColumn) {
            this.sortAsc = !this.sortAsc;
        } else {
            this.sortColumn = sort;
            this.sortAsc = true;
        }
        this.load();
    }

    private getParams(): PapersSearchParams {
        return {
            page: this.page,
            size: this.size,
            sort: this.sortColumn,
            asc: this.sortAsc,
            conditions: this.buildSearchConditions()
        };
    }

    private buildSearchConditions() {
        const allFilters = [];
        if (this.term && this.term !== '') {
            allFilters.push(SearchFilter.equals('term', this.term));
        }
        if (this.state) {
            allFilters.push(SearchFilter.equals('state', this.state));
        }
        if (this.published === 'yes') {
            allFilters.push(SearchFilter.equals('published', true));
        } else if (this.published === 'no') {
            allFilters.push(SearchFilter.equals('published', false));
        }
        if (this.company != null && this.company !== '') {
            allFilters.push(SearchFilter.equals('speaker.company', this.company));
        }

        const anyFilters = [];
        if (this.excludeCanceled) {
            this.states
                .filter(it => it != 'canceled')
                .forEach(it => anyFilters.push(SearchFilter.equals('state', it)));
        }

        return {all: allFilters, any: anyFilters};
    }

    load() {
        loader.emit(true);
        this.talkService
            .search(this.getParams())
            .then(searchResult => {
                loader.emit(false);
                this.storeParams();
                this.searchResult = searchResult;
                this.searchResult.votersCount = searchResult.votersCount;
                this.searchResult.page = this.page;
                this.searchResult.totalPages = Math.floor(searchResult.total / this.size);
                if (searchResult.total % this.size !== 0) {
                    this.searchResult.totalPages = this.searchResult.totalPages + 1;
                }
                this.pages = [];
                for (let i = 0; i < searchResult.totalPages; i++) {
                    this.pages.push(i);
                }
            });
    }

    formatSpeakersNames(item: Talk): string {
        const speakers = item.speakers;
        if (speakers.length === 0) {
            return '';
        }
        if (speakers.length === 1) {
            return speakers[0].fullName;
        } else {
            return speakers.map(s => s.fullName).join(', ');
        }
    }

    updateState(talk: Talk, state) {
        loader.emit(true);
        this.talkService.updateState(talk, state).then(result => {
            loader.emit(false);
            if (result) {
                this.errorService.successEvents.emit('Operation Success');
                this.load();
            }
        });
    }

    updatePublished(talk: Talk, published) {
        loader.emit(true);
        this.talkService.updatePublished(talk, published).then(result => {
            loader.emit(false);
            if (result) {
                this.errorService.successEvents.emit('Operation Success');
                this.load();
            }
        });
    }

    updateResponsiblePersonModal(talk: Talk) {
        swal({
            title: 'Select a name of Juger as a person of contact',
            input: 'select',
            inputOptions: this.inputOptions,
            confirmButtonText: 'Approve this person',
            cancelButtonText: 'Cancel',
            showCancelButton: true
        }).then((name) => {
                if (name.value !== null && name.value !== undefined) {
                    this.updateResponsiblePerson(talk, name.value);
                }
            }
        );
    }

    updateResponsiblePerson(talk: Talk, name) {
        talk.responsiblePerson = name;
        this.talkService.update(talk).then((result) => {
            if (result) {
                this.load();
            }
        });
    }

    toggleFormat() {
        this.format = this.formatAsHtml ? 'html' : 'source';
    }

    private getCsvParams(): PapersSearchParams {
        return {
            page: 0,
            size: 1000,
            sort: 'speakers.fullName',
            asc: true,
            conditions: this.buildSearchConditions()
        };
    }


    saveAsCSV() {
        this.talkService
            .search(this.getCsvParams())
            .then(result => {
                Csv.of(result.items)
                    .saveAsCSV('talks.csv',
                        [
                            'type', 'title', 'state', 'preferenceDay', 'languages',
                            'First speaker name', 'First speaker email', 'First speaker twitter',
                            'Second speaker name', 'Second speaker email', 'Second speaker twitter',
                            '_id'
                        ],
                        (value: Talk) => {
                            return [
                                value.type,
                                value.title,
                                value.state,
                                value.preferenceDay,
                                value.languages.sort().join(","),
                                value.speakers[0].fullName,
                                value.speakers[0].email,
                                value.speakers[0].twitter,
                                value.speakers.length === 2 ? value.speakers[1].fullName : '',
                                value.speakers.length === 2 ? value.speakers[1].email : '',
                                value.speakers.length === 2 ? value.speakers[1].twitter : '',
                                value._id
                            ];
                        });
            });
    }
}
