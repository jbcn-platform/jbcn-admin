export const menu = [
    {text: 'Dashboard', link: '/home', icon: 'icon-speedometer', permissions: ['role:voter', 'role:helper', 'role:admin']},
    {text: 'Papers', link: '/papers/list', icon: 'icon-notebook', permissions: ['role:voter', 'role:helper', 'role:admin']},
    {text: 'Talks', link: '/talks/list', icon: 'icon-notebook', permissions: ['role:voter', 'role:helper', 'role:admin']},

    {text: 'Voting', heading: true, permissions: ['role:voter', 'role:admin']},
    {text: 'My votes', link: '/papers/votes/dashboard', icon: 'icon-notebook', permissions: ['role:voter', 'role:admin']},
    {text: 'Inbox', link: '/papers/votes/inbox', icon: 'icon-exclamation', permissions: ['role:voter', 'role:admin']},

    {text: 'Back office', heading: true, permissions: ['role:admin']},
    {text: 'Sponsors', link: '/sponsors/list', icon: 'icon-organization', permissions: ['role:admin', 'role:helper']},
    {text: 'JobOffers', link: '/job-offers/list', icon: 'icon-organization', permissions: ['role:admin', 'role:helper']},
    {text: 'Tracks', link: '/tracks/list', icon: 'icon-home', permissions: ['role:admin']},
    {text: 'Rooms', link: '/rooms/list', icon: 'icon-home', permissions: ['role:admin']},
    {text: 'Sessions', link: '/sessions/list', icon: 'icon-calendar', permissions: ['role:admin', 'role:helper']},
    {text: 'Attendees votes', link: '/attendees-votes/list', icon: 'icon-like', permissions: ['role:admin']},
    /* TODO find better section? */
    {text: 'Url redirects', link: '/url-redirects/list', icon: 'icon-link', permissions: ['role:admin']},

    {text: 'Sponsors', heading: true, permissions: ['role:sponsor']},
    {text: 'Badges', link: '/badges/list', icon: 'icon-user', permissions: ['role:sponsor']},

    {text: 'Administration', heading: true, permissions: ['role:admin']},
    {text: 'Users', link: '/users/list', icon: 'icon-user', permissions: ['role:admin']}
];
