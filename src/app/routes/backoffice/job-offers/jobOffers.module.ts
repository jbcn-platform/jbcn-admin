import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {JobOffersListComponent} from './list/job-offers-list.component';
import {JobOffersFormComponent} from './form/job-offers-form.component';
import {JobOfferService} from '@core/backend/job-offers-sponsors.service';
import {BackofficeSponsorsService} from '@core/backend/backoffice-sponsors.service';
import {JobOffersJsonComponent} from './json/job-offers-json.component';

const routes: Routes = [
    {
        path: '', children: [
            {path: 'list', component: JobOffersListComponent},
            {path: 'form', component: JobOffersFormComponent},
            {path: 'form/:id', component: JobOffersFormComponent},
            {path: 'json', component: JobOffersJsonComponent},
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
    ],
    exports: [],
    declarations: [
        JobOffersListComponent,
        JobOffersFormComponent,
        JobOffersJsonComponent,
    ],
    providers: [
        JobOfferService,
        BackofficeSponsorsService,
    ],
})
export class JobOffersModule {
}
