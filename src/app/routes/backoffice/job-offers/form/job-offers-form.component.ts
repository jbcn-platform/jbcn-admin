import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorService} from '@core/error/error.service';
import {BackofficeSponsor, JobOffer} from '@shared/model/backoffice';
import {JobOfferService} from '@core/backend/job-offers-sponsors.service';
import {BackofficeSponsorsService} from '@core/backend/backoffice-sponsors.service';

@Component({
    selector: 'app-job-offer-form',
    templateUrl: 'job-offers-form.component.html',
    styleUrls: ['job-offers-form.component.scss']
})
export class JobOffersFormComponent implements OnInit {

    jobOffer = new JobOffer();
    sponsors: BackofficeSponsor[] = null;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private errorService: ErrorService,
        private offersService: JobOfferService,
        private sponsorsService: BackofficeSponsorsService
    ) {
    }

    ngOnInit() {
        const id = this.route.snapshot.params['id'];
        if (id) {
            this.offersService.get(id).then(offer => {
                this.jobOffer = offer as JobOffer;
            });
        }
        this.sponsorsService.getAll().then(results => {
            this.sponsors = results;
        });
    }

    create() {
        if (this.isValidJobOffer()) {
            this.offersService.create(this.jobOffer).then(() => {
                this.errorService.successEvents.emit('operation.success');
                this.router.navigate(['job-offers', 'list']);
            });
        }
    }

    update() {
        if (this.isValidJobOffer()) {
            this.offersService.update(this.jobOffer).then(() => {
                this.errorService.successEvents.emit('operation.success');
                this.router.navigate(['job-offers', 'list']);
            });
        }
    }

    private isValidJobOffer() {
        if (!this.jobOffer.title) {
            this.errorService.errorEvents.emit('error.name.mandatory');
            return false;
        }
        if (!this.jobOffer.description) {
            this.errorService.errorEvents.emit('error.description.mandatory');
            return false;
        }
        if (!this.jobOffer.link) {
            this.errorService.errorEvents.emit('error.link.mandatory');
            return false;
        }
        if (!this.jobOffer.sponsorId) {
            this.errorService.errorEvents.emit('error.sponsor.mandatory');
            return false;
        }
        return true;
    }

    updateSponsorId(sponsorId: string) {
        // console.log(sponsorId);
        this.jobOffer.sponsorId = sponsorId;
        // console.log(this.jobOffer);
    }
}
