import {Component, OnInit} from '@angular/core';
import {loader} from '@shared/util/eventEmitters';
import {SearchResult} from '@shared/model/models';
import {BackofficeSponsor, JobOffer} from '@shared/model/backoffice';
import {JobOfferService} from '@core/backend/job-offers-sponsors.service';
import {BackofficeSponsorsService} from '@core/backend/backoffice-sponsors.service';

const swal = require('sweetalert2');

@Component({
    selector: 'app-job-offer-list',
    templateUrl: 'job-offers-list.component.html',
    styleUrls: ['job-offers-list.component.scss']
})
export class JobOffersListComponent implements OnInit {

    searchResult = new SearchResult<JobOffer>();
    sponsors = new Map<string, Object>();

    constructor(
        private offersService: JobOfferService,
        private sponsorsService: BackofficeSponsorsService
    ) {
    }

    ngOnInit() {
        this.load();
    }

    search() {
        this.load();
    }

    load() {
        loader.emit(true);
        this.sponsorsService.getAll().then(results => {
            for (const i in results) {
                // typescript shenanigans
                const s = results[i] as BackofficeSponsor;
                this.sponsors.set(s._id, s);
            }
            this.offersService.getAll().then(searchResult => {
                this.searchResult.items = searchResult as JobOffer[];
                loader.emit(false);
            });
        });
    }

    deleteModal(item: JobOffer) {
        swal({
            title: 'Are you sure?',
            text: `Do you want to delete: ${item.title}?`,
            type: 'warning',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Abort',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                this.delete(item._id);
            }
        });
    }

    delete(id: string) {
        this.offersService.delete(id).then(() => {
            this.load();
        });
    }
}
