import {Component, OnInit} from '@angular/core';
import {BackofficeSponsor, JobOffer} from '@shared/model/backoffice';
import {JobOfferService} from '@core/backend/job-offers-sponsors.service';
import {BackofficeSponsorsService} from '@core/backend/backoffice-sponsors.service';
import {loader} from '@shared/util/eventEmitters';

@Component({
    selector: 'app-job-offer-json',
    templateUrl: 'job-offers-json.component.html',
    styleUrls: ['job-offers-json.component.scss']
})
export class JobOffersJsonComponent implements OnInit {

    sponsors = new Map<string, BackofficeSponsor>();
    offersBySponsor = new Map<string, JobOffer[]>();

    data = '';
    lines = 40;

    constructor(
        private offersService: JobOfferService,
        private sponsorsService: BackofficeSponsorsService
    ) {
    }

    ngOnInit() {
        loader.emit(true);
        // TODO wait for all all once
        this.sponsorsService.getAll().then(sponsorsResult => {
            (sponsorsResult as BackofficeSponsor[])
                .forEach(it => {
                    this.sponsors.set(it._id, it);
                });
            this.offersService.getAll().then(offersResult => {
                (offersResult as JobOffer[])
                    .forEach(it => {
                        this.offersBySponsor.forEach((k, v) => {
                        });
                        if (this.offersBySponsor.has(it.sponsorId)) {
                            this.offersBySponsor.get(it.sponsorId).push(it);
                        } else {
                            let aux = new Array();
                            aux.push(it);
                            this.offersBySponsor.set(it.sponsorId, aux);
                        }
                    });
                this.data = this.mapOffers();
                loader.emit(false);
            });
        });
    }

    private mapOffers() {
        let webOffers = new Array();
        this.offersBySponsor.forEach((offers, sponsorId) => {
            let jobOfferDTO = new JobOfferDTO();
            let sponsor = this.sponsors.get(sponsorId);
            jobOfferDTO.company = this.mapCompany(sponsor);
            jobOfferDTO.links = this.mapLinks(sponsor);
            jobOfferDTO.vacancies = offers.map(offer => this.mapVacancy(offer));
            webOffers.push(jobOfferDTO);
        });

        return JSON.stringify(webOffers, null, '\t');
    }

    private mapCompany(sponsor: BackofficeSponsor): CompanyDTO {
        const companyDto = new CompanyDTO();
        companyDto.name = sponsor.name;
        companyDto.description = sponsor.description;
        // links is auto-mapped by Json response, so it's not a real Typescript Map
        companyDto['logo-url'] = sponsor.links['logo'];
        companyDto.level = sponsor.level;
        return companyDto;
    }

    private mapLinks(sponsor: BackofficeSponsor) {
        const linksDto = new LinksDTO();
        linksDto.web = sponsor.links['web'];
        linksDto.linkedIn = sponsor.links['linkedIn'];
        linksDto.twitter = sponsor.links['twitter'];
        linksDto.instagram = sponsor.links['instagram'];
        return linksDto;
    }

    private mapVacancy(offer: JobOffer) {
        const vacancyDto = new VacancyDTO();
        vacancyDto.title = offer.title;
        vacancyDto.description = offer.description;
        vacancyDto.location = offer.location;
        vacancyDto.link = offer.link;
        return vacancyDto;
    }
}

class JobOfferDTO {
    company: CompanyDTO;
    links: LinksDTO;
    vacancies: VacancyDTO[];
}

class CompanyDTO {
    name: string;
    description: string;
    'logo-url': string;
    level: string;
}

class LinksDTO {
    web: string;
    linkedIn: string;
    twitter: string;
    instagram: string;
}

class VacancyDTO {
    title: string;
    description: string;
    location: string;
    link: string;
}

