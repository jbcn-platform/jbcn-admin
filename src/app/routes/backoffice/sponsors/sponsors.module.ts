import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SponsorsListComponent} from './list/sponsors-list.component';
import {SponsorsFormComponent} from './form/sponsors-form.component';
import {BackofficeSponsorsService} from '@core/backend/backoffice-sponsors.service';

const routes: Routes = [
    {
        path: '', children: [
            {path: 'list', component: SponsorsListComponent},
            {path: 'form', component: SponsorsFormComponent},
            {path: 'form/:id', component: SponsorsFormComponent}
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    exports: [],
    declarations: [
        SponsorsListComponent,
        SponsorsFormComponent
    ],
    providers: [
        BackofficeSponsorsService
    ],
})
export class SponsorsModule {
}
