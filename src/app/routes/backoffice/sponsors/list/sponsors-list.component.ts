import {Component, OnInit} from '@angular/core';
import {loader} from '@shared/util/eventEmitters';
import {SearchResult} from '@shared/model/models';
import {BackofficeSponsor} from '@shared/model/backoffice';
import {BackofficeSponsorsService} from '@core/backend/backoffice-sponsors.service';

const swal = require('sweetalert2');

@Component({
    selector: 'app-sponsors-list',
    templateUrl: 'sponsors-list.component.html',
    styleUrls: ['sponsors-list.component.scss']
})
export class SponsorsListComponent implements OnInit {

    searchResult = new SearchResult<BackofficeSponsor>();

    constructor(private service: BackofficeSponsorsService) {
    }

    ngOnInit() {
        this.load();
    }

    search() {
        this.load();
    }

    load() {
        loader.emit(true);
        this.service.getAll().then(searchResult => {
            this.searchResult.items = searchResult as BackofficeSponsor[];
            loader.emit(false);
        });
    }

    deleteModal(item: BackofficeSponsor) {
        swal({
            title: 'Are you sure?',
            text: `Do you want to delete: ${item.name} (${item.level})?`,
            type: 'warning',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Abort',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                this.delete(item._id);
            }
        });
    }

    delete(id: string) {
        this.service.delete(id).then(() => {
            this.load();
        });
    }
}
