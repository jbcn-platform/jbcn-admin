import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorService} from '@core/error/error.service';
import {BackofficeSponsor} from '@shared/model/backoffice';
import {BackofficeSponsorsService} from '@core/backend/backoffice-sponsors.service';

@Component({
    selector: 'app-sponsors-form',
    templateUrl: 'sponsors-form.component.html',
    styleUrls: ['sponsors-form.component.scss']
})
export class SponsorsFormComponent implements OnInit {

    sponsor = new BackofficeSponsor();
    links = new Map<string, string>();

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private errorService: ErrorService,
        private service: BackofficeSponsorsService
    ) {
    }

    ngOnInit() {
        const id = this.route.snapshot.params['id'];
        if (id) {
            this.service.get(id).then(sponsor => {
                this.sponsor = sponsor as BackofficeSponsor;
                this.links = sponsor.links;
            });
        }
    }

    create() {
        if (this.isValidSponsor()) {
            this.sponsor.links = this.links;
            this.service.create(this.sponsor).then(() => {
                this.errorService.successEvents.emit('operation.success');
                this.router.navigate(['sponsors', 'list']);
            });
        }
    }

    update() {
        if (this.isValidSponsor()) {
            this.sponsor.links = this.links;
            this.service.update(this.sponsor).then(() => {
                this.errorService.successEvents.emit('operation.success');
                this.router.navigate(['sponsors', 'list']);
            });
        }
    }

    private isValidSponsor() {
        if (!this.sponsor.name) {
            this.errorService.errorEvents.emit('error.name.mandatory');
            return false;
        }
        if (!this.sponsor.description) {
            this.errorService.errorEvents.emit('error.description.mandatory');
            return false;
        }
        if (!this.sponsor.level) {
            this.errorService.errorEvents.emit('error.level.mandatory');
            return false;
        }
        return true;
    }
}
