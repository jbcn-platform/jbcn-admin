import {Component, OnInit} from '@angular/core';
import {loader} from '@shared/util/eventEmitters';
import {SearchResult} from '@shared/model/models';
import {UrlRedirect} from '@shared/model/urlRedirect';
import {UrlRedirectsService} from '@core/backend/url-redirects.service';
import {environment} from '@environments/environment';

const swal = require('sweetalert2');

@Component({
    selector: 'app-redirects-list',
    templateUrl: 'redirects-list.component.html',
    styleUrls: ['redirects-list.component.scss']
})
export class RedirectsListComponent implements OnInit {

    searchResult = new SearchResult<UrlRedirect>();

    constructor(private service: UrlRedirectsService) {
    }

    ngOnInit() {
        this.load();
    }

    search() {
        this.load();
    }

    load() {
        loader.emit(true);
        this.service.getAll().then(searchResult => {
            this.searchResult.items = searchResult as UrlRedirect[];
            loader.emit(false);
        });
    }

    deleteModal(item: UrlRedirect) {
        swal({
            title: 'Are you sure?',
            text: `Do you want to delete: ${item.name} (${item.url})?`,
            type: 'warning',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Abort',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                this.delete(item._id);
            }
        });
    }

    delete(id: string) {
        this.service.delete(id).then(() => {
            this.load();
        });
    }

    call_redirect(item: UrlRedirect) {
        this.goToLink(`${environment.apiHost}/r/${item.name}`);
    }

    private goToLink(url: string) {
        window.open(url, '_blank');
    }
}
