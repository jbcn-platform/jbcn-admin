import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {RedirectsListComponent} from './list/redirects-list.component';
import {RedirectsFormComponent} from './form/redirects-form.component';
import {UrlRedirectsService} from '@core/backend/url-redirects.service';

const routes: Routes = [
    {
        path: '', children: [
            {path: 'list', component: RedirectsListComponent},
            {path: 'form', component: RedirectsFormComponent},
            {path: 'form/:id', component: RedirectsFormComponent}
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    exports: [],
    declarations: [
        RedirectsListComponent,
        RedirectsFormComponent
    ],
    providers: [
        UrlRedirectsService
    ],
})
export class RedirectsModule {
}
