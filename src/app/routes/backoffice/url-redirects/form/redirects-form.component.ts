import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorService} from '@core/error/error.service';
import {UrlRedirect} from '@shared/model/urlRedirect';
import {UrlRedirectsService} from '@core/backend/url-redirects.service';

@Component({
    selector: 'app-redirects-form',
    templateUrl: 'redirects-form.component.html',
    styleUrls: ['redirects-form.component.scss']
})
export class RedirectsFormComponent implements OnInit {

    redirect = new UrlRedirect();

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private errorService: ErrorService,
        private service: UrlRedirectsService
    ) {
    }

    ngOnInit() {
        const id = this.route.snapshot.params['id'];
        if (id) {
            this.service.get(id).then(it => {
                this.redirect = it as UrlRedirect;
            });
        }
    }

    create() {
        if (this.isValidRedirect()) {
            this.service.create(this.redirect).then(() => {
                this.errorService.successEvents.emit('operation.success');
                this.router.navigate(['url-redirects', 'list']);
            });
        }
    }

    update() {
        if (this.isValidRedirect()) {
            this.service.update(this.redirect).then(() => {
                this.errorService.successEvents.emit('operation.success');
                this.router.navigate(['url-redirects', 'list']);
            });
        }
    }

    private isValidRedirect() {
        if (!this.redirect.url) {
            this.errorService.errorEvents.emit('error.url.mandatory');
            return false;
        }
        if (!this.redirect.name) {
            this.errorService.errorEvents.emit('error.name.mandatory');
            return false;
        }
        return true;
    }
}
