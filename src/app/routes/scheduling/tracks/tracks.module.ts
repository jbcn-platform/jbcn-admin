import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TracksListComponent } from './list/tracks-list.component';
import { TracksFormComponent } from './form/tracks-form.component';
import { TracksService } from '@core/backend/tracks.service';

// Component based on `Rooms` component
const routes: Routes = [
    {
        path: '', children: [
            { path: 'list', component: TracksListComponent },
            { path: 'form', component: TracksFormComponent },
            { path: 'form/:id', component: TracksFormComponent }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    exports: [],
    declarations: [
        TracksListComponent,
        TracksFormComponent
    ],
    providers: [
        TracksService
    ],
})
export class TracksModule { }
