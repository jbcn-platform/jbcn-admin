import { Component, OnInit } from '@angular/core';
import { loader } from '@shared/util/eventEmitters';
import { Track, SearchResult } from '@shared/model/models';
import { TracksService } from "@core/backend/tracks.service";

const swal = require('sweetalert2');

@Component({
    selector: 'app-tracks-list',
    templateUrl: 'tracks-list.component.html',
    styleUrls: ['tracks-list.component.scss']
})
export class TracksListComponent implements OnInit {

    searchResult = new SearchResult<Track>();

    constructor(private service: TracksService) {
    }

    ngOnInit() {
        this.load();
    }

    search() {
        this.load();
    }

    load() {
        loader.emit(true);
        this.service.getAll().then(searchResult => {
            this.searchResult.items = searchResult as Track[];
            loader.emit(false);
        });
    }

    deleteModal(item: Track) {
        swal({
            title: 'Are you sure?',
            text: 'Do you want to delete track: ' + item.name + '?',
            type: 'warning',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Abort',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                this.delete(item._id)
            }
        });
    }

    delete(id: string) {
        this.service.delete(id).then(result => {
            this.load()
        });
    }

}
