import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorService } from '../../../../core/error/error.service';
import { Track } from '@shared/model/models';
import { TracksService } from '@core/backend/tracks.service';

@Component({
    selector: 'app-tracks-form',
    templateUrl: 'tracks-form.component.html',
    styleUrls: ['tracks-form.component.scss']
})

export class TracksFormComponent implements OnInit {

    readonly urlContext = 'tracks';

    track: Track = new Track();

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private errorService: ErrorService,
        private service: TracksService
    ) { }

    ngOnInit() {
        var id = this.route.snapshot.params['id']
        if (id) {
            this.service.get(id).then(track => {
                this.track = track as Track
            });
        }
    }

    create() {
        if (this.isValidTrack()) {
            this.service.create(this.track).then(response => {
                this.errorService.successEvents.emit('operation.success');
                this.router.navigate([this.urlContext, 'list']);
            });
        }
    }

    update() {
        if (this.isValidTrack()) {
            this.service.update(this.track).then(response => {
                this.errorService.successEvents.emit('operation.success');
                this.router.navigate([this.urlContext, 'list']);
            });
        }
    }

    private isValidTrack() {
        if (!this.track.name) {
            this.errorService.errorEvents.emit('error.name.mandatory');
            return false;
        }
        return true;
    }

}
