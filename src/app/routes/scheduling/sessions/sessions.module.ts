import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SessionsListComponent } from './list/sessions-list.component';
import { SessionsFormComponent } from './form/sessions-form.component';
import { SessionsService } from '@core/backend/sessions.service';
import { RoomsService } from '@core/backend/rooms.service';
import { TracksService } from '@core/backend/tracks.service';
import { TalkService } from '@core/backend/talk.service';

const routes: Routes = [
    {
        path: '', children: [
            { path: 'list', component: SessionsListComponent },
            { path: 'form', component: SessionsFormComponent },
            { path: 'form/:id', component: SessionsFormComponent }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    exports: [],
    declarations: [
        SessionsListComponent,
        SessionsFormComponent
    ],
    providers: [
        RoomsService,
        TracksService,
        SessionsService,
        TalkService
    ],
})
export class SessionsModule { }
