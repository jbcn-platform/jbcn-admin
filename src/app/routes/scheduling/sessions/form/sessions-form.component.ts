import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { loader } from '@shared/util/eventEmitters';
import { ErrorService } from '@core/error/error.service';
import { Session, Track, Room } from '@shared/model/scheduling';
import { SessionsService } from '@core/backend/sessions.service';
import { RoomsService } from '@core/backend/rooms.service';
import { TracksService } from '@core/backend/tracks.service';
import { TalkService } from '@core/backend/talk.service';
import { ISearchParams, Talk, SearchFilter } from '@shared/model/models';

@Component({
    selector: 'app-sessions-form',
    templateUrl: 'sessions-form.component.html',
    styleUrls: ['sessions-form.component.scss']
})
export class SessionsFormComponent implements OnInit {

    instance = new Session();
    rooms: Room[];
    tracks: Track[];
    talks: Talk[];
    talk: Talk = new Talk();

    originalTalkId: string;

    edit: Boolean = true;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private errorService: ErrorService,
        private service: SessionsService,
        private roomsService: RoomsService,
        private tracksService: TracksService,
        private talksService: TalkService
    ) { }

    ngOnInit() {
        this.talk = new Talk()
        const id = this.route.snapshot.params['id'];
        if (id) {
            this.edit = false;
            this.service.get(id).then(instance => {
                this.instance = instance;
                if (!instance.track) {
                    this.instance.track = new Track();
                }
                this.initTalk(id);
            });
        }
        this.roomsService.getAll().then(response => {
            this.rooms = response
        });
        this.tracksService.getAll().then(response => {
            this.tracks = response
        });
        this.talksService.search({
            size: 100,
            sort: "title",
            asc: true,
            fields: ["title", "type", "sessionId"]
        } as ISearchParams).then(response => {
            this.talks = response.items
        })
    }

    private initTalk(sessionId: string) {
        this.talksService.search({
            size: 1,
            filters: [SearchFilter.equals("sessionId", sessionId)],
            asc: true,
            fields: ["title", "type"]
        } as ISearchParams).then(response => {
            // using embedded talk in Session, fron crashes...no idea why
            this.talk = response.items[0]
            this.originalTalkId = response.items[0]._id
        })
    }

    create() {
        if (this.isValid()) {
            this.service.create(this.instance).then(response => {
                this.errorService.successEvents.emit('operation.success');
                this.router.navigate(['sessions', 'list']);
                if (this.talk._id != null) {
                    this.updateTalkSession(this.instance._id, response)
                }
            });
        }
    }

    update() {
        if (this.isValid()) {
            this.service.update(this.instance).then(response => {
                this.errorService.successEvents.emit('operation.success');
                if (this.originalTalkId != this.instance._id) {
                    this.updateTalkSession(this.talk._id, this.instance._id)
                }

                this.router.navigate(['sessions', 'list']);
            });
        }
    }

    private updateTalkSession(talkId: string, sessionId: string) {
        const update = { _id: talkId, sessionId: sessionId }
        this.talksService.update(update as Talk).then(result => {
        })
    }

    private isValid() {
        if (!this.instance.slotId) {
            this.errorService.errorEvents.emit('error.slotId.mandatory');
            return false;
        }
        if (!this.instance.day) {
            this.errorService.errorEvents.emit('error.day.mandatory');
            return false;
        }
        return true;
    }

}
