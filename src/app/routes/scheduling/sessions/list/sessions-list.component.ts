import { Component, OnInit } from '@angular/core';
import { loader } from '@shared/util/eventEmitters';
import { Session, SearchResult, Room, Track, Talk, ISearchParams, SearchFilter } from '@shared/model/models';
import { SessionsService } from "@core/backend/sessions.service";
import { RoomsService } from '@core/backend/rooms.service';
import { TracksService } from '@core/backend/tracks.service';
import { TalkService } from '@core/backend/talk.service';
import { isUndefined } from 'util';

const swal = require('sweetalert2');

@Component({
    selector: 'app-sessions-list',
    templateUrl: 'sessions-list.component.html',
    styleUrls: ['sessions-list.component.scss']
})
export class SessionsListComponent implements OnInit {

    searchResult = new SearchResult<Session>();
    rooms: Room[];
    tracks: Track[];
    talks: Talk[];

    constructor(
        private service: SessionsService,
        private roomsService: RoomsService,
        private tracksService: TracksService,
        private talksService: TalkService) {
    }

    ngOnInit() {
        this.load()
    }

    search() {
        this.load()
    }

    load() {
        loader.emit(true)
        this.service.getAll().then(searchResult => {
            this.searchResult.items = searchResult

            this.initTalks()
            this.roomsService.getAll().then(response => {
                this.rooms = response
                this.searchResult.items.forEach(it => {
                    it.room = response.filter(r => r._id == it.roomId)[0]
                })
            });
            this.tracksService.getAll().then(response => {
                this.tracks = response
                this.searchResult.items.forEach(it => {
                    it.track = response.filter(t => t._id == it.trackId)[0]
                })
            });

            loader.emit(false)
        });
    }

    private initTalks() {
        this.talksService.search({
            size: 100,
            sort: "title",
            asc: true,
            fields: ["title", "type", "sessionId"]
        } as ISearchParams).then(response => {
            this.talks = response.items
            this.searchResult.items.forEach(it => {
                it.talks = this.talks.filter(t => t.sessionId == it._id)
            })
        })
    }

    deleteModal(item: Session) {
        swal({
            title: 'Are you sure?',
            text: 'Do you want to delete session: ' + item.slotId + "in " + item.day + '?',
            type: 'warning',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Abort',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                this.delete(item._id)
            }
        });
    }

    delete(id: string) {
        this.service.delete(id).then(result => {
            this.load()
        });
    }

    updateSession(talkId: string, sessionId: string) {
        this.updateSessionAndRefresh(talkId, sessionId, true)
    }

    clearTalks(sessionId: string) {
        this.talksService.search({
            size: 50,
            filters: [{
                name: "sessionId",
                value: sessionId
            } as SearchFilter],
            asc: true,
            fields: ["title", "type"]
        } as ISearchParams).then(response => {
            response.items.forEach(it => {
                this.updateSessionAndRefresh(it._id, "null", false)
            })
            this.load()
        })
    }

    private updateSessionAndRefresh(talkId: string, sessionId: string, refreshView: boolean) {
        const update = { _id: talkId, sessionId: sessionId }
        this.talksService.update(update as Talk).then(result => {
            if (refreshView)
                this.load()
        })
    }

}
