import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RoomsListComponent } from './list/rooms-list.component';
import { RoomsFormComponent } from './form/rooms-form.component';
import { RoomsService } from '@core/backend/rooms.service';

const routes: Routes = [
    {
        path: '', children: [
            { path: 'list', component: RoomsListComponent },
            { path: 'form', component: RoomsFormComponent },
            { path: 'form/:id', component: RoomsFormComponent }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    exports: [],
    declarations: [
        RoomsListComponent,
        RoomsFormComponent
    ],
    providers: [
        RoomsService
    ],
})
export class RoomsModule { }
