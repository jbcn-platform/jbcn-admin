import { Component, OnInit } from '@angular/core';
import { loader } from '@shared/util/eventEmitters';
import { Room, SearchResult } from '@shared/model/models';
import { RoomsService } from '@core/backend/rooms.service';

const swal = require('sweetalert2');

@Component({
    selector: 'app-rooms-list',
    templateUrl: 'rooms-list.component.html',
    styleUrls: ['rooms-list.component.scss']
})
export class RoomsListComponent implements OnInit {

    searchResult = new SearchResult<Room>();

    constructor(private service: RoomsService) {
    }

    ngOnInit() {
        this.load();
    }

    search() {
        this.load();
    }

    load() {
        loader.emit(true);
        this.service.getAll().then(searchResult => {
            this.searchResult.items = searchResult as Room[];
            loader.emit(false);
        });
    }

    deleteModal(item: Room) {
        swal({
            title: 'Are you sure?',
            text: 'Do you want to delete room: ' + item.name + '?',
            type: 'warning',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Abort',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                this.delete(item._id)
            }
        });
    }

    delete(id: string) {
        this.service.delete(id).then(result => {
            this.load()
        });
    }

}
