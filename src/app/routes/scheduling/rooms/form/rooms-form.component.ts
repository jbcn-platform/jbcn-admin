import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorService } from '@core/error/error.service';
import { Room } from '@shared/model/scheduling';
import { RoomsService } from '@core/backend/rooms.service';

@Component({
    selector: 'app-rooms-form',
    templateUrl: 'rooms-form.component.html',
    styleUrls: ['rooms-form.component.scss']
})
export class RoomsFormComponent implements OnInit {

    room: Room = new Room();

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private errorService: ErrorService,
        private service: RoomsService
    ) { }

    ngOnInit() {
        const id = this.route.snapshot.params['id'];
        if (id) {
            this.service.get(id).then(room => {
                this.room = room as Room;
            });
        }
    }

    create() {
        if (this.isValidRoom()) {
            this.service.create(this.room).then(response => {
                this.errorService.successEvents.emit('operation.success');
                this.router.navigate(['rooms', 'list']);
            });
        }
    }

    update() {
        if (this.isValidRoom()) {
            this.service.update(this.room).then(response => {
                this.errorService.successEvents.emit('operation.success');
                this.router.navigate(['rooms', 'list']);
            });
        }
    }

    private isValidRoom() {
        if (!this.room.name) {
            this.errorService.errorEvents.emit('error.name.mandatory');
            return false;
        }
        return true;
    }

}
