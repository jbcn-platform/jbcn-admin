import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService } from '../../../core/settings/settings.service';
import { AuthService } from '../../../core/auth/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { loader } from '../../../shared/util/eventEmitters';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    valForm: FormGroup;
    errorMessage: string;

    constructor(
        public settings: SettingsService,
        private fb: FormBuilder,
        private authService: AuthService,
        private router: Router
    ) {
        this.valForm = fb.group({
            'username': [null, Validators.required],
            'password': [null, Validators.required]
        });
    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        this.errorMessage = null;
        for (const c of Object.keys(this.valForm.controls)) {
            this.valForm.controls[c].markAsTouched();
        }

        if (this.valForm.valid) {
            loader.emit(true);
            this.authService.login(value.username, value.password).then(response => {
                loader.emit(false);
                this.router.navigate(['']);
            }, error => {
                loader.emit(false);
                this.errorMessage = 'Authentication error';
            });
        }
    }

    ngOnInit() {
        if (this.authService.isLogged()) {
            this.router.navigate(['']);
        }
    }

}
