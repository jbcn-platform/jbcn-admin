import {Component, OnInit} from '@angular/core';
import {DashboardService} from '@core/dashboard/dashboard.service';
import {PaperService} from '@core/backend/paper.service';
import {TalkService} from '@core/backend/talk.service';
import {AuthService} from '@core/auth/auth.service';
import {environment} from 'environments/environment';
import {Role} from '@core/auth/role';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    papersCount = 0;
    talksCount = 0;
    speakersCount = 0;
    speakersNeedCashCount = 0;
    tagCounters = [];
    maxPapersValue = 0;
    maxTalksValue = 0;
    Math: any;
    talkCounter: any = {};

    readonly edition = environment.edition;

    constructor(
        private paperService: PaperService,
        private talkService: TalkService,
        private dashboardService: DashboardService,
        private authService: AuthService
    ) {
        this.Math = Math;
    }

    ngOnInit() {
        if (this.authService.hasRole(Role.ADMIN, Role.VOTER, Role.HELPER)) {
            const currentEdition = environment.edition.year;
            this.paperService.count(currentEdition)
                .then(count => {
                    this.papersCount = count;
                });

            this.talkService.count(currentEdition)
                .then(counter => {
                    this.talkCounter = counter;
                    this.talksCount = counter.talks;
                    this.speakersCount = counter.speakers;
                    this.speakersNeedCashCount = counter.speakersCash;
                });

            this.dashboardService.countTags(currentEdition)
                .then(counter => {
                    const charData = [{
                        'label': 'Papers',
                        'color': '#2f80e7',
                        'data': []
                    }, {
                        'label': 'Talks',
                        'color': '#2b957a',
                        'data': []
                    }];
                    const keys = Object.keys(counter);
                    const counters = [];
                    console.log(counter);
                    for (const tag of keys) {
                        // this.tagChartData[1].data.push([tag, counter[tag].talk]);
                        counters.push({'tag': tag, 'paper': counter[tag].paper, 'talk': counter[tag].talk});
                    }

                    this.tagCounters = counters.sort((a, b) => {
                        if (!a.talk) {
                            a.talk = 0;
                        }
                        if (!b.talk) {
                            b.talk = 0;
                        }
                        if (a.talk < b.talk) {
                            return 1;
                        }

                        if (a.talk > b.talk) {
                            return -1;
                        }
                        if (a.talk === b.talk) {
                            return 0;
                        }
                    });
                    this.maxPapersValue = Math.max.apply(Math, counters.map((c) => c.paper || 0));
                    this.maxTalksValue = this.tagCounters.length === 0 ? 0 : this.tagCounters[0].talk;
                });
        }
    }
}
